<div class="td-footer-wrapper td-container-wrap td-footer-template-2 <?php echo td_util::get_option('td_full_footer'); ?>">
    <div class="td-container">

	    <div class="td-pb-row">
		    <div class="td-pb-span12">
			    <?php
                $tds_footer_top_title = td_util::get_option('tds_footer_top_title');
                // ad spot
                echo td_global_blocks::get_instance('td_block_ad_box')->render(array('spot_id' => 'footer_top', 'spot_title' => $tds_footer_top_title));
                ?>
		    </div>
	    </div>

        <div class="td-pb-row">

            <div class="td-pb-span4">
                <a href="https://edumap.az/"><img src="https://edumap.az/wp-content/uploads/2016/07/logo-for-random-dude-533x261.png"></src></a>
            </div>

            <div class="td-pb-span4">
                <div class="footer-text-wrap">Edumap.az kollektivi olaraq biz gəncləri, cəmiyyətimizi təhsil sahəsindəki yeniliklər barədə məlumatlandırmaq, layihə, təlim, seminar və xaricdəki təhsil imkanları barədə yenilikləri onlarla paylaşmaq məqsədi ilə saytı ərsəyə gətirmişik. <div class="footer-email-wrap">Bizimlə əlaqə: <a href="mailto:info@edumap.az">info@edumap.az</a></div></div>
				<div class="footer-social-wrap td-social-style-2">
        <span class="td-social-icon-wrap">
            <a target="_blank" href="https://www.facebook.com/edumap.az" title="Facebook">
                <i class="td-icon-font td-icon-facebook"></i>
            </a>
        </span>
        <span class="td-social-icon-wrap">
            <a target="_blank" href="https://plus.google.com/114502790235324379143" title="Google+">
                <i class="td-icon-font td-icon-googleplus"></i>
            </a>
        </span>
        <span class="td-social-icon-wrap">
            <a target="_blank" href="https://www.instagram.com/edumap.az/" title="Instagram">
                <i class="td-icon-font td-icon-instagram"></i>
            </a>
        </span>
        <span class="td-social-icon-wrap">
            <a target="_blank" href="https://twitter.com/edumapaz" title="Twitter">
                <i class="td-icon-font td-icon-twitter"></i>
            </a>
        </span>
        <span class="td-social-icon-wrap">
            <a target="_blank" href="https://www.youtube.com/channel/UCfzJDfRt9jrYbiAptAbBNpQ" title="Youtube">
                <i class="td-icon-font td-icon-youtube"></i>
            </a>
        </span></div>
            </div>

            <div class="td-pb-span4">
                <a href="http://millinet.az/" target="_blank" ><img src="https://edumap.az/wp-content/uploads/2017/11/mlogo_1.png"></src></a>
            </div>
        </div>
    </div>
</div>
