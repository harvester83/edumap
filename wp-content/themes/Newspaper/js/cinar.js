(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.ssMetadata = [
		{name:"cinar", frames: [[0,0,614,614],[1218,0,600,600],[616,0,600,600],[616,602,600,600],[0,1204,924,128],[926,1204,924,128]]}
];


// symbols:



(lib.Banner1 = function() {
	this.spriteSheet = ss["cinar"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.Banner2 = function() {
	this.spriteSheet = ss["cinar"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.Banner3 = function() {
	this.spriteSheet = ss["cinar"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.Banner4 = function() {
	this.spriteSheet = ss["cinar"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap1 = function() {
	this.spriteSheet = ss["cinar"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.flash0ai = function() {
	this.spriteSheet = ss["cinar"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween21 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00A551").s().p("AihCvQgBgFABgDQACgCAFgBIATgHQAWgIgEgMIgEgJQgDgIACgIQABgHAEgIIACgEQABAAAAAAQABAAABAAQAAAAABABQAAAAABAAIAEAFQACACAEABQAFABANgBIAQAAQAJgCAHACIAcAGIAMAAQgEgDAFgDQAFgDANAAQAogBAfgKQAcgIgFgFIgDgCIgBAAQgMgCgMgQIgKgRQgMgFgcgFQgSgDgBgIQgBgGAFgLQABgIAKgIQALgKARgFQAHgDAIgBIAFgBQAJgEgCgHQgDgGgIgBQgIgCgBgHQAAgGAFgCQAcgDASgrQAGgQgBgKQAAgKgJAGQgSANgaAFQgaAFAEgIQADgHgCgFQgCgEgDgBIgEAFIgKAKIAAAAIgEADQgPANgRAHIgEABIgDgBIAVgOQAKgKAEgEQADgEAFgIIAIgRQAGgLAHgDQAIAWAqgJQAugJADACQAGAHgMAnQgNAnAFAEQAIAHACAFQACAGgFAAQggACgFAKQgEAJAQAGQAMAFACAFIABAEQgYgDgWAIIgDAAQgRAFgKAMQgMAMASAIQANAFAXACIAFgBIAAABIAHgBQADAYAQAOQAMAKAVAIIALADQgZARguAMQgnAJgVAAQgFAAAAABIADADQAEAFACALIgIgEQgQgIgfgFQgRgEgUAAIgSABIgSABQgIgBgDACQgDADAAAFIABARQABAKgFAFQgEAEgNACQgJACgGAHIgFAGQgDADgDAAQgGAAgDgIg");
	this.shape.setTransform(-12.1,-7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00904D").s().p("AgQAPIgCgCIgBgDIgBgBQgCgDACgCQABgCAGAAIAEAAIAEgCQAEgBACgEIAGgIQAFgDAEABQAEACACAGQABAHgDABQgBACgEAAQgHACgIAGIgIADIgFACIgBAAIgCgBg");
	this.shape_1.setTransform(-25.8,14.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#00904D").s().p("AgOAPIgDAAIgCgDIgCgHQABgBAEgCIAEgBIAIgEQAGgCAFgFIAAgBIABgDQABgCAEACIACABIACAAIADABIACADIgBACIgFAGQgHACgDADIgFAIQgDADgDAAIgGABIgDgBg");
	this.shape_2.setTransform(-25.9,11.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#EA202A").s().p("Ag2A4QgHAAgCgDQgBgCACgDQABgCADgBIAHgCQADgBAAgEQABgCgCgFIgCgQQAAgGABgJIACgOIgBgPIgCgPQAAgFABgBQADgDAGAAIAJAAIAKgEQALgDAOAFIADACIACACIAAAAQABAHgMgDQgNgBgOAFIgDACQgCACAAAEIABAHIADA1QAAAIAGAGIACACIAiAAIAJABQAGAAADgCQACgCABgEQACgLgBgVQgBgUACgKQAAgBABAAQABAAAAAAQABAAAAAAQABABAAABIACAEIADAOQAFAdgDAOQgBAEABABQAAAAABABQAAAAABAAQAAABABAAQAAAAABABIAJACQABAAABAAQAAAAABAAQABAAAAAAQAAABABAAIABADIAAAEIgBAHQhNgHgoAEg");
	this.shape_3.setTransform(-26,12.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#EC1C24").s().p("ABpCJQgDgLgCgDIgDgDIgEgDQgHgEgMADIAAAAIgDABQAKgTAGgaQAGgZgCAAIgigGIAFgRQABgTgPgNIgYgWQgVgVgCgPIABgKIAAgBIACgLQgBgFgCABIgKAPQgGAIgDAGQgEAKgCAMQgBAGgLAGIgQALQgPAOADAVIAGATIgiAGQgDAAAHAYQAHAaAKASIgKAAIgIABQgFADgDAEIgGAMIgCAGQAAABAAABQgBAAAAABQgBAAAAAAQgBABAAAAQgCAAABgYQABgXADgCQAFgEAFAEQADACgQgcQgOgiAJgWQARASAQgHQAHgMgUgzIAOAAQAPgCALgGQANgIAHgNQACgDABgIQACgGADgDQASgSAIghIABAAQAFAHACAHQAFAIACAPIADAXQAEATALAKQAOANAWADIALABQALABAAgCQgJAbgDARQgDAPADAEQAMAFAKgHIADgDIABABIAHgIQAKAWgOAiQgPAdADgCQAFgDAFACQAEACABAFQABAKgBALIAAAKQgBAHgCAEIgBAAQgBAAgCgHg");
	this.shape_4.setTransform(-26.1,-33.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#EA202A").s().p("AgBBEQAAAAgBgBQAAAAAAAAQgBgBAAAAQAAgBAAAAIABhEIgCAAIAAAAIgEgDIgFgFQgEgHAAgGQgCgKAEgPIABgBQABgGADgEQAFgIAIABQAHACADAHIACADIAAABIACAIIABALQABAGgCAGIgCALQgDAGgGADIAAACIABACIgBArIgDANIAAACIAAABIgCAGQAAAAAAABQAAAAgBAAQAAABAAAAQAAAAAAAAgAgCgyIgCAIIAAAJIADAKIABACIABADIAAABIABAAIADgEIABgBIACgEIACgHIAAgBQAAgGgDgEIgEgGIgBgBIgCgDg");
	this.shape_5.setTransform(-25.7,31.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#EA202A").s().p("AgfByIgBgBIgCgCIAAgCIADg4IAIgkQAGgZALgUQAGgMAHgIIAJgJIAJgGIgKgaQgEgLACgNIAAAAIADgBIABABIAAAIIABAJIAPAdIACAGIAAABIAAABIgBAAIAAABIAAAAIgBAAIgHAFIgIAIQgHAJgFALQgHAQgIAbIgPBcIAAAAIAAABIAAABIgBAAIAAABIgCABIgCABg");
	this.shape_6.setTransform(-22.2,33.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#EA202A").s().p("AAgBrIAAAAIgahMQgGgVgJgOIgKgRIgNgOIAAAAIgBgCIgBgCIABgEIABgCIAMgTQALgPgDgOQAAgBAAgBQAAAAgBgBQAAAAAAAAQgBgBAAAAIgOgHQAAAAgBAAQAAAAAAgBQAAAAAAAAQAAAAABgBIABAAIALABQAHABADAEQADADACAIQABAIgDAJIgHAPIgGAPIALALQAGAIADAHIAOAfIAIAhIAJA6IgBABIAAAAg");
	this.shape_7.setTransform(-29.5,32.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#EA202A").s().p("AgkAMIgCgBIgBgBIAAgBIgBgBIAAAAIACgDIAEgBQAdABAWAAIAagCIALAAIgFgBIgTgBIhhgDIgPABIgCABIABAAIACABQABAAAAABQAAAAAAAAQAAAAAAABQAAAAAAAAQAAAAgBAAQAAABAAAAQAAAAgBAAQAAAAAAgBIgEgCIgCgBIAAgBIACgCQAJgEANgBIAWgCQAPgCAcAAIAsADIAeAEIACAAIAGADIABAAIADACQABAAAAAAQAAABAAAAQABABAAAAQAAABAAAAIgBADIgBACQgEAEgKAAIhqABg");
	this.shape_8.setTransform(-25.9,20.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#0071BB").s().p("AgsCRIADgEQAAgBgFAAQgUACgkgJQgqgJgZgPQAbgKALgHIAAAAIAHgEQAagQADgaQAHAAASgBQAXgCAMgFIAHgDQAMgHgKgLQgQgQgfgDIgdAAIAAgEQADgGALgEQAQgGgEgJQgFgKgggCQgFABACgHQACgFAHgHQAFgEgNgnQgOgnAGgGQADgEAuAKQArAHAIgWQAGACAGAIQAEAEAEAJQADAJADAEIALANIAVAWQAAAAAAABQAAAAAAAAQgBAAgBAAQgBAAgBAAIgMgHQgPgHgMgOIgKgOQgCgBgDAGQgDAFADAHQAEAJgZgEQgZgEgRgMQgJgHgBAKQAAAKAHAQQARArAbABQAGACgBAGQAAAIgIABQgLADABAIQAAAIATACQATAEAPAKQANALABAKQABALgIAFQgIAJgVAEQgXAFgNAEQgSAXgOAIQgJAIgGABQADAGAWAFQAZAHAhAAQAOAAAFACQAEACgDAEIAAABQACAAAJgCIAwgIIALAAQAFABALAFQARAJAMgEIARgBQAKgCACgKQAJgpgCgRQgCgSgOgNIgUgQQgSgMAAgKIAAABIAAgBQAJAKAQAGIAbANIAEABQAOgogFggIAAgCIgBgWIgHgXQAPATAFATQAEAQgBAZIgHBLQgBACAEAdQACAWgHALIgFAPQgCAJgGAEQgEACgZAEIgeADIgLABIgLAAQgOgFABgCQAHgIgFgDQgGgDgWADQgfAFgNADQgPADgTAJQACgJAEgFg");
	this.shape_9.setTransform(-41.6,-9.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#2E3191").s().p("AgNAaIABgFIAIgCIAAghIgIgBIgBgDIABgGIAPgDIACABIAAAsIAIABIABACQAAACgCADIgYABg");
	this.shape_10.setTransform(57.3,4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#2E3191").s().p("AgRAbIABgGIAHgCIAAghIgJgBIgBgCIABgHIAQgCIABAMIABAAQAEgNAOAAIACACIgCALIgDAAQgKAAgFAIIAAAZIAMAAIABACQAAADgCADIgbABg");
	this.shape_11.setTransform(53.4,3.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#2E3191").s().p("AgXAQQABgPAcgBIAAgOQgFgDgHAAQgHAAgHADIgCgCIAAgGQAJgFALAAQAKAAAIAGIAAAoIAGAAIABACIgBAGIgNABIgBgCIAAgFIgBAAQgFAHgKAAQgPAAAAgMgAgNALIAAABIAAABQAAAHAIAAQAFAAAGgEIAAgIIgFAAQgKAAgEADg");
	this.shape_12.setTransform(48.7,3.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#2E3191").s().p("AgMAkIABgFIAIgCIAAg3IgKgBIgCgCIABgFIASgDIABAKIAAA3IAJABIACACIgCAFIgZABg");
	this.shape_13.setTransform(44.1,3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#2E3191").s().p("AAaAaIACgHIAAgiQgDgCgFAAQgKAAgEAJIAAAbIAMAAIACACIgCAGIgVABIgCgCIACgHIAAgiQgDgCgFAAQgJAAgGAJIAAAbIAOAAIABACIgCAGIgeABIgBgBIABgGIAHgBIAAgjIgIAAIgBgCIABgHIARgCIABAJIABAAQAEgKALAAQALAAAFAKIABAAQAEgKALAAQAKAAAHAGIAAAoIAFAAIACACIgCAGIgRABg");
	this.shape_14.setTransform(37.9,3.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#2E3191").s().p("AgMAaIABgFIAIgCIAAghIgIgBIgCgDIABgGIAPgDIADABIAAAsIAHABIABACIgCAFIgXABg");
	this.shape_15.setTransform(31.6,4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#2E3191").s().p("AgXAnIgEgIQAQgHAJgJIgIgCIgMgrIgGAAIgBgDIACgFIAYgBIABABIgBAFIgHADIAIAkIADAAQAIgOACgWIgHAAIgBgDQAAgCADgDIAXgBIABABIgBAFIgFACQgGAXgJAQQgJAVgUALg");
	this.shape_16.setTransform(26.8,5.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#2E3191").s().p("AgXAQQABgPAcgBIAAgOQgFgDgGAAQgIAAgHADIgCgCIAAgGQAJgFALAAQAKAAAIAGIAAAoIAGAAIABACIgBAGIgNABIgBgCIAAgFIgBAAQgEAHgLAAQgPAAAAgMgAgNALIAAABIAAABQAAAHAIAAQAFAAAGgEIAAgIIgFAAQgKAAgEADg");
	this.shape_17.setTransform(21.2,3.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#2E3191").s().p("AgXAnQgCgCgCgGQAQgHAIgJIgHgCIgMgrIgGAAIgBgDIACgFIAYgBIABABIgBAFIgIADIAJAkIADAAQAIgOADgWIgHAAIgCgDIACgFIAYgBIABABIgBAFIgGACQgEAWgKARQgJAWgUAKg");
	this.shape_18.setTransform(15.3,5.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#EC1C24").s().p("AhQB5QABgOAEgJIAfgJIAAicQgRgHgZAAIgGgJQABgJAEgSQAlgLAkgBQAGAZABAeIAFAAQATg9BBAAIALAJQAAAegHAYIgSgBQgpAAgaAgIAAB4IA4ADIAGAKQgDANgGAMQhCAFg/AAg");
	this.shape_19.setTransform(49.3,24.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#EC1C24").s().p("AhZBzQgTgQAAgaQAAgjAjgTQAigTBDgCIAAhCQgXgOgiAAQgfAAghAMIgJgFQgEgMACgQQAmgaA2AAQA0AAAiAcIAAC6IAdADIAFAKIgBAMIgDANIg9AEIgHgKIACgVIgFgBQgLARgVAJQgSAJgXAAQgfAAgSgPgAgkAoQgSAFgIAHIgBAEIAAACQAAAiAlAAQAeAAAYgSIAAgmIgXgBQgVAAgUAFg");
	this.shape_20.setTransform(27.6,24.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#EC1C24").s().p("AAvB2QAGgPAEgRIAAibQgQgNgYAAQgUAAgTAMQgRAMgJASIAAB+IA9ADIAGAKQgDANgGAMQhDAFhEAAIgEgIQAAgLAEgMIAggIIAAigQgMgDgZgBIgGgJQAAgJAFgSQAlgKAngCQAFAXABASIAEAAQATguA3gBQArABAfAbIAAC7IAcACIAGAKQgCANgFAMIhOAFg");
	this.shape_21.setTransform(0.8,24.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#EC1C24").s().p("AgkC3IgEgKQADgTAFgJQAKAIATAEQASAEAOgEQgDgSghgFIgGgKIAGghQgigFgVgVQgdgfAAg7QAAhDAggmQAegjAsgBQATABAGAFIAFgBIABgaIASgDQALAAAIADIAJAKQAAA0gOA8QgTAEgQgGQACgjgBgRQgQgEgRAAQgUAAgRAGQgTAjAAAwQAABUBMAAQAYAAAXgGIAJAGQADAIABASQghAPglACIgBAKQASACAMANQAMALAAARQAAATgPANQgPANgWAAQgUAAgagIg");
	this.shape_22.setTransform(-48.4,28.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-58.6,-47.5,117.3,95);


(lib.Tween20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00A551").s().p("AihCvQgBgFABgDQACgCAFgBIATgHQAWgIgEgMIgEgJQgDgIACgIQABgHAEgIIACgEQABAAAAAAQABAAABAAQAAAAABABQAAAAABAAIAEAFQACACAEABQAFABANgBIAQAAQAJgCAHACIAcAGIAMAAQgEgDAFgDQAFgDANAAQAogBAfgKQAcgIgFgFIgDgCIgBAAQgMgCgMgQIgKgRQgMgFgcgFQgSgDgBgIQgBgGAFgLQABgIAKgIQALgKARgFQAHgDAIgBIAFgBQAJgEgCgHQgDgGgIgBQgIgCgBgHQAAgGAFgCQAcgDASgrQAGgQgBgKQAAgKgJAGQgSANgaAFQgaAFAEgIQADgHgCgFQgCgEgDgBIgEAFIgKAKIAAAAIgEADQgPANgRAHIgEABIgDgBIAVgOQAKgKAEgEQADgEAFgIIAIgRQAGgLAHgDQAIAWAqgJQAugJADACQAGAHgMAnQgNAnAFAEQAIAHACAFQACAGgFAAQggACgFAKQgEAJAQAGQAMAFACAFIABAEQgYgDgWAIIgDAAQgRAFgKAMQgMAMASAIQANAFAXACIAFgBIAAABIAHgBQADAYAQAOQAMAKAVAIIALADQgZARguAMQgnAJgVAAQgFAAAAABIADADQAEAFACALIgIgEQgQgIgfgFQgRgEgUAAIgSABIgSABQgIgBgDACQgDADAAAFIABARQABAKgFAFQgEAEgNACQgJACgGAHIgFAGQgDADgDAAQgGAAgDgIg");
	this.shape.setTransform(-12.1,-7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00904D").s().p("AgQAPIgCgCIgBgDIgBgBQgCgDACgCQABgCAGAAIAEAAIAEgCQAEgBACgEIAGgIQAFgDAEABQAEACACAGQABAHgDABQgBACgEAAQgHACgIAGIgIADIgFACIgBAAIgCgBg");
	this.shape_1.setTransform(-25.8,14.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#00904D").s().p("AgOAPIgDAAIgCgDIgCgHQABgBAEgCIAEgBIAIgEQAGgCAFgFIAAgBIABgDQABgCAEACIACABIACAAIADABIACADIgBACIgFAGQgHACgDADIgFAIQgDADgDAAIgGABIgDgBg");
	this.shape_2.setTransform(-25.9,11.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#EA202A").s().p("Ag2A4QgHAAgCgDQgBgCACgDQABgCADgBIAHgCQADgBAAgEQABgCgCgFIgCgQQAAgGABgJIACgOIgBgPIgCgPQAAgFABgBQADgDAGAAIAJAAIAKgEQALgDAOAFIADACIACACIAAAAQABAHgMgDQgNgBgOAFIgDACQgCACAAAEIABAHIADA1QAAAIAGAGIACACIAiAAIAJABQAGAAADgCQACgCABgEQACgLgBgVQgBgUACgKQAAgBABAAQABAAAAAAQABAAAAAAQABABAAABIACAEIADAOQAFAdgDAOQgBAEABABQAAAAABABQAAAAABAAQAAABABAAQAAAAABABIAJACQABAAABAAQAAAAABAAQABAAAAAAQAAABABAAIABADIAAAEIgBAHQhNgHgoAEg");
	this.shape_3.setTransform(-26,12.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#EC1C24").s().p("ABpCJQgDgLgCgDIgDgDIgEgDQgHgEgMADIAAAAIgDABQAKgTAGgaQAGgZgCAAIgigGIAFgRQABgTgPgNIgYgWQgVgVgCgPIABgKIAAgBIACgLQgBgFgCABIgKAPQgGAIgDAGQgEAKgCAMQgBAGgLAGIgQALQgPAOADAVIAGATIgiAGQgDAAAHAYQAHAaAKASIgKAAIgIABQgFADgDAEIgGAMIgCAGQAAABAAABQgBAAAAABQgBAAAAAAQgBABAAAAQgCAAABgYQABgXADgCQAFgEAFAEQADACgQgcQgOgiAJgWQARASAQgHQAHgMgUgzIAOAAQAPgCALgGQANgIAHgNQACgDABgIQACgGADgDQASgSAIghIABAAQAFAHACAHQAFAIACAPIADAXQAEATALAKQAOANAWADIALABQALABAAgCQgJAbgDARQgDAPADAEQAMAFAKgHIADgDIABABIAHgIQAKAWgOAiQgPAdADgCQAFgDAFACQAEACABAFQABAKgBALIAAAKQgBAHgCAEIgBAAQgBAAgCgHg");
	this.shape_4.setTransform(-26.1,-33.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#EA202A").s().p("AgBBEQAAAAgBgBQAAAAAAAAQgBgBAAAAQAAgBAAAAIABhEIgCAAIAAAAIgEgDIgFgFQgEgHAAgGQgCgKAEgPIABgBQABgGADgEQAFgIAIABQAHACADAHIACADIAAABIACAIIABALQABAGgCAGIgCALQgDAGgGADIAAACIABACIgBArIgDANIAAACIAAABIgCAGQAAAAAAABQAAAAgBAAQAAABAAAAQAAAAAAAAgAgCgyIgCAIIAAAJIADAKIABACIABADIAAABIABAAIADgEIABgBIACgEIACgHIAAgBQAAgGgDgEIgEgGIgBgBIgCgDg");
	this.shape_5.setTransform(-25.7,31.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#EA202A").s().p("AgfByIgBgBIgCgCIAAgCIADg4IAIgkQAGgZALgUQAGgMAHgIIAJgJIAJgGIgKgaQgEgLACgNIAAAAIADgBIABABIAAAIIABAJIAPAdIACAGIAAABIAAABIgBAAIAAABIAAAAIgBAAIgHAFIgIAIQgHAJgFALQgHAQgIAbIgPBcIAAAAIAAABIAAABIgBAAIAAABIgCABIgCABg");
	this.shape_6.setTransform(-22.2,33.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#EA202A").s().p("AAgBrIAAAAIgahMQgGgVgJgOIgKgRIgNgOIAAAAIgBgCIgBgCIABgEIABgCIAMgTQALgPgDgOQAAgBAAgBQAAAAgBgBQAAAAAAAAQgBgBAAAAIgOgHQAAAAgBAAQAAAAAAgBQAAAAAAAAQAAAAABgBIABAAIALABQAHABADAEQADADACAIQABAIgDAJIgHAPIgGAPIALALQAGAIADAHIAOAfIAIAhIAJA6IgBABIAAAAg");
	this.shape_7.setTransform(-29.5,32.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#EA202A").s().p("AgkAMIgCgBIgBgBIAAgBIgBgBIAAAAIACgDIAEgBQAdABAWAAIAagCIALAAIgFgBIgTgBIhhgDIgPABIgCABIABAAIACABQABAAAAABQAAAAAAAAQAAAAAAABQAAAAAAAAQAAAAgBAAQAAABAAAAQAAAAgBAAQAAAAAAgBIgEgCIgCgBIAAgBIACgCQAJgEANgBIAWgCQAPgCAcAAIAsADIAeAEIACAAIAGADIABAAIADACQABAAAAAAQAAABAAAAQABABAAAAQAAABAAAAIgBADIgBACQgEAEgKAAIhqABg");
	this.shape_8.setTransform(-25.9,20.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#0071BB").s().p("AgsCRIADgEQAAgBgFAAQgUACgkgJQgqgJgZgPQAbgKALgHIAAAAIAHgEQAagQADgaQAHAAASgBQAXgCAMgFIAHgDQAMgHgKgLQgQgQgfgDIgdAAIAAgEQADgGALgEQAQgGgEgJQgFgKgggCQgFABACgHQACgFAHgHQAFgEgNgnQgOgnAGgGQADgEAuAKQArAHAIgWQAGACAGAIQAEAEAEAJQADAJADAEIALANIAVAWQAAAAAAABQAAAAAAAAQgBAAgBAAQgBAAgBAAIgMgHQgPgHgMgOIgKgOQgCgBgDAGQgDAFADAHQAEAJgZgEQgZgEgRgMQgJgHgBAKQAAAKAHAQQARArAbABQAGACgBAGQAAAIgIABQgLADABAIQAAAIATACQATAEAPAKQANALABAKQABALgIAFQgIAJgVAEQgXAFgNAEQgSAXgOAIQgJAIgGABQADAGAWAFQAZAHAhAAQAOAAAFACQAEACgDAEIAAABQACAAAJgCIAwgIIALAAQAFABALAFQARAJAMgEIARgBQAKgCACgKQAJgpgCgRQgCgSgOgNIgUgQQgSgMAAgKIAAABIAAgBQAJAKAQAGIAbANIAEABQAOgogFggIAAgCIgBgWIgHgXQAPATAFATQAEAQgBAZIgHBLQgBACAEAdQACAWgHALIgFAPQgCAJgGAEQgEACgZAEIgeADIgLABIgLAAQgOgFABgCQAHgIgFgDQgGgDgWADQgfAFgNADQgPADgTAJQACgJAEgFg");
	this.shape_9.setTransform(-41.6,-9.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#2E3191").s().p("AgNAaIABgFIAIgCIAAghIgIgBIgBgDIABgGIAPgDIACABIAAAsIAIABIABACQAAACgCADIgYABg");
	this.shape_10.setTransform(57.3,4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#2E3191").s().p("AgRAbIABgGIAHgCIAAghIgJgBIgBgCIABgHIAQgCIABAMIABAAQAEgNAOAAIACACIgCALIgDAAQgKAAgFAIIAAAZIAMAAIABACQAAADgCADIgbABg");
	this.shape_11.setTransform(53.4,3.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#2E3191").s().p("AgXAQQABgPAcgBIAAgOQgFgDgHAAQgHAAgHADIgCgCIAAgGQAJgFALAAQAKAAAIAGIAAAoIAGAAIABACIgBAGIgNABIgBgCIAAgFIgBAAQgFAHgKAAQgPAAAAgMgAgNALIAAABIAAABQAAAHAIAAQAFAAAGgEIAAgIIgFAAQgKAAgEADg");
	this.shape_12.setTransform(48.7,3.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#2E3191").s().p("AgMAkIABgFIAIgCIAAg3IgKgBIgCgCIABgFIASgDIABAKIAAA3IAJABIACACIgCAFIgZABg");
	this.shape_13.setTransform(44.1,3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#2E3191").s().p("AAaAaIACgHIAAgiQgDgCgFAAQgKAAgEAJIAAAbIAMAAIACACIgCAGIgVABIgCgCIACgHIAAgiQgDgCgFAAQgJAAgGAJIAAAbIAOAAIABACIgCAGIgeABIgBgBIABgGIAHgBIAAgjIgIAAIgBgCIABgHIARgCIABAJIABAAQAEgKALAAQALAAAFAKIABAAQAEgKALAAQAKAAAHAGIAAAoIAFAAIACACIgCAGIgRABg");
	this.shape_14.setTransform(37.9,3.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#2E3191").s().p("AgMAaIABgFIAIgCIAAghIgIgBIgCgDIABgGIAPgDIADABIAAAsIAHABIABACIgCAFIgXABg");
	this.shape_15.setTransform(31.6,4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#2E3191").s().p("AgXAnIgEgIQAQgHAJgJIgIgCIgMgrIgGAAIgBgDIACgFIAYgBIABABIgBAFIgHADIAIAkIADAAQAIgOACgWIgHAAIgBgDQAAgCADgDIAXgBIABABIgBAFIgFACQgGAXgJAQQgJAVgUALg");
	this.shape_16.setTransform(26.8,5.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#2E3191").s().p("AgXAQQABgPAcgBIAAgOQgFgDgGAAQgIAAgHADIgCgCIAAgGQAJgFALAAQAKAAAIAGIAAAoIAGAAIABACIgBAGIgNABIgBgCIAAgFIgBAAQgEAHgLAAQgPAAAAgMgAgNALIAAABIAAABQAAAHAIAAQAFAAAGgEIAAgIIgFAAQgKAAgEADg");
	this.shape_17.setTransform(21.2,3.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#2E3191").s().p("AgXAnQgCgCgCgGQAQgHAIgJIgHgCIgMgrIgGAAIgBgDIACgFIAYgBIABABIgBAFIgIADIAJAkIADAAQAIgOADgWIgHAAIgCgDIACgFIAYgBIABABIgBAFIgGACQgEAWgKARQgJAWgUAKg");
	this.shape_18.setTransform(15.3,5.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#EC1C24").s().p("AhQB5QABgOAEgJIAfgJIAAicQgRgHgZAAIgGgJQABgJAEgSQAlgLAkgBQAGAZABAeIAFAAQATg9BBAAIALAJQAAAegHAYIgSgBQgpAAgaAgIAAB4IA4ADIAGAKQgDANgGAMQhCAFg/AAg");
	this.shape_19.setTransform(49.3,24.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#EC1C24").s().p("AhZBzQgTgQAAgaQAAgjAjgTQAigTBDgCIAAhCQgXgOgiAAQgfAAghAMIgJgFQgEgMACgQQAmgaA2AAQA0AAAiAcIAAC6IAdADIAFAKIgBAMIgDANIg9AEIgHgKIACgVIgFgBQgLARgVAJQgSAJgXAAQgfAAgSgPgAgkAoQgSAFgIAHIgBAEIAAACQAAAiAlAAQAeAAAYgSIAAgmIgXgBQgVAAgUAFg");
	this.shape_20.setTransform(27.6,24.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#EC1C24").s().p("AAvB2QAGgPAEgRIAAibQgQgNgYAAQgUAAgTAMQgRAMgJASIAAB+IA9ADIAGAKQgDANgGAMQhDAFhEAAIgEgIQAAgLAEgMIAggIIAAigQgMgDgZgBIgGgJQAAgJAFgSQAlgKAngCQAFAXABASIAEAAQATguA3gBQArABAfAbIAAC7IAcACIAGAKQgCANgFAMIhOAFg");
	this.shape_21.setTransform(0.8,24.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#EC1C24").s().p("AgkC3IgEgKQADgTAFgJQAKAIATAEQASAEAOgEQgDgSghgFIgGgKIAGghQgigFgVgVQgdgfAAg7QAAhDAggmQAegjAsgBQATABAGAFIAFgBIABgaIASgDQALAAAIADIAJAKQAAA0gOA8QgTAEgQgGQACgjgBgRQgQgEgRAAQgUAAgRAGQgTAjAAAwQAABUBMAAQAYAAAXgGIAJAGQADAIABASQghAPglACIgBAKQASACAMANQAMALAAARQAAATgPANQgPANgWAAQgUAAgagIg");
	this.shape_22.setTransform(-48.4,28.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-58.6,-47.5,117.3,95);


(lib.Tween17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#9E0355","#A4095A","#C42D79","#DC468F","#EA569C","#EF5BA1"],[0,0.043,0.318,0.58,0.812,1],-452.1,0,447.9,0).s().p("A/LI7Qlggojxh9QkWiPhqj1Qikl5rFiYQjdgvj5gTQhegIhEgCMCL8AAAIAAQYQjOAGiYgyQiAgqjggMQkqgRkGBAQlXBTmXgZQjLgPhegEQihgGh5AUQhsASnqADQoLADhDAGQjnAahBAGQhcAJiDgMQlogpi6gPQlMgbh6AlQjjBFksAcQijAPiaAAQinAAidgRg");
	this.shape.setTransform(2.2,-21.4,1,0.748);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("ADCNjQlngvi7gSQlLggh6ArQi5BClTgOQi6gInng4QnUg2j+gHQmQgLkUBHQirArlZALQkNAIk6gNIAA67MCMnAAAIAAaGQjOAHiXg6Qh/gyjhgOQkqgTkGBKQjOA6jMAOQiWAKi/gOQjKgShfgEQihgHh4AYQhtAVnqADQoMADhBAHIkoAmQgnAEgvAAQg/AAhLgIg");
	this.shape_1.setTransform(0,0,1,0.748);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-450,-65.4,900,131);


(lib.Tween16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#9E0355","#A4095A","#C42D79","#DC468F","#EA569C","#EF5BA1"],[0,0.043,0.318,0.58,0.812,1],-452.1,0,447.9,0).s().p("A/LI7Qlggojxh9QkWiPhqj1Qikl5rFiYQjdgvj5gTQhegIhEgCMCL8AAAIAAQYQjOAGiYgyQiAgqjggMQkqgRkGBAQlXBTmXgZQjLgPhegEQihgGh5AUQhsASnqADQoLADhDAGQjnAahBAGQhcAJiDgMQlogpi6gPQlMgbh6AlQjjBFksAcQijAPiaAAQinAAidgRg");
	this.shape.setTransform(2.2,-21.5,1,0.748);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("ADCNjQlngvi7gSQlLggh6ArQi5BClTgOQi6gInng4QnUg2j+gHQmQgLkUBHQirArlZALQkNAIk6gNIAA67MCMnAAAIAAaGQjOAHiXg6Qh/gyjhgOQkqgTkGBKQjOA6jMAOQiWAKi/gOQjKgShfgEQihgHh4AYQhtAVnqADQoMADhBAHIkoAmQgnAEgvAAQg/AAhLgIg");
	this.shape_1.setTransform(0,0,1,0.748);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-450,-65.5,900,131);


(lib.Tween13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Banner4();
	this.instance.parent = this;
	this.instance.setTransform(-200.6,-200.8,0.669,0.669);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-200.6,-200.8,401.4,401.6);


(lib.Tween12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Banner4();
	this.instance.parent = this;
	this.instance.setTransform(-200.6,-200.7,0.669,0.669);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-200.6,-200.7,401.4,401.6);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Banner4();
	this.instance.parent = this;
	this.instance.setTransform(93.7,-49,0.163,0.163);

	this.instance_1 = new lib.Banner3();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-1.3,-49,0.163,0.163);

	this.instance_2 = new lib.Banner2();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-95.3,-50,0.163,0.163);

	this.instance_3 = new lib.Banner1();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-191.3,-50,0.163,0.163);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-191.3,-50,382.7,100);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Banner4();
	this.instance.parent = this;
	this.instance.setTransform(93.7,-49,0.163,0.163);

	this.instance_1 = new lib.Banner3();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-1.3,-49,0.163,0.163);

	this.instance_2 = new lib.Banner2();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-95.3,-50,0.163,0.163);

	this.instance_3 = new lib.Banner1();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-191.3,-50,0.163,0.163);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-191.3,-50,382.7,100);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Bitmap1();
	this.instance.parent = this;
	this.instance.setTransform(-462,-64);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(-462,-64,924,128), null);


// stage content:
(lib.Banner900x100Cinarreklam = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.movieClip_1.addEventListener("click", fl_ClickToGoToWebPage_2);
		
		function fl_ClickToGoToWebPage_2() {
			window.open("http://cinaryayimlari.com", "_blank");
		}
		this.movieClip_1.addEventListener("click", fl_ClickToGoToWebPage);
		
		function fl_ClickToGoToWebPage() {
			window.open("http://cinaryayimlari.com", "_blank");}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(229));

	// Layer 8
	this.instance = new lib.Tween20("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-63.8,49.5);
	this.instance._off = true;

	this.instance_1 = new lib.Tween21("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(64.2,49.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance}]},146).to({state:[{t:this.instance_1}]},7).wait(76));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(146).to({_off:false},0).to({_off:true,x:64.2,y:49.6},7).wait(76));

	// Layer 7
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgNBLQgHgGAAgLQAAgJAGgFQAGgHAIAAQAJAAAGAHQAGAFAAAJQAAALgGAGQgGAGgJAAQgHAAgGgGgAgOgrQgGgHAAgJQAAgJAHgGQAGgGAHAAQAIAAAHAGQAGAGAAAJQAAAJgHAHQgGAGgIABQgIgBgGgGg");
	this.shape.setTransform(477.5,39.6,1.793,1.793);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAjBKQgFgFAAgNIAAhAQAAgUgGgLQgFgLgPAAQgIAAgJAHQgJAHgEAKQgCAKAAAXIAAAxQAAAMgGAHQgFAGgIAAQgJAAgFgHQgFgFAAgNIAAhyQAAgLAFgFQAEgGAJAAQAFAAAEACQAEADACAFQACAEAAAIIAAAFQAKgPAMgFQAKgHAQAAQAPAAAMAHQALAHAGAMQAEAHABAJIABAVIAABJQAAAMgFAHQgFAGgJAAQgIAAgFgHg");
	this.shape_1.setTransform(454.3,39.6,1.793,1.793);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAlBlQgFgFAAgLIAAgFQgHAIgHAHQgJAGgHADQgJADgKAAQgOAAgLgGQgLgGgGgLQgHgNAAgXIAAhOQAAgMAGgGQAEgGAJAAQAIAAAGAGQAEAGAAAMIAAA/QAAAMADALQACAKAGAFQAGAFAJAAQAJAAAIgGQAIgFAFgLQACgIABgdIAAgvQAAgLAFgHQAFgGAIAAQAJAAAFAGQAFAFAAANIAAByQAAAMgFAFQgEAGgJAAQgHAAgFgGgAAJhOQgFgEAAgIQAAgGAFgFQAFgFAGAAQAEAAAEACQADACACAEIADAIQAAAIgFAEQgFAFgGAAQgFAAgGgFgAgrhOQgFgEAAgIQAAgHAFgFQAEgEAGAAQAGAAAFAEQAFAEAAAIQAAAIgFAEQgFAFgGAAQgFAAgFgFg");
	this.shape_2.setTransform(423.8,34.9,1.793,1.793);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgjBjQAAgDACgCQAAAAABgBQAAAAABAAQAAAAABgBQABAAAAAAIAHABIAKAAQAYAAAAgMQAAgFgFgDQgDgDgFAAIgMABQgEAAgCgCQgCgDAAgDQAAgHAUgDQgigCgRgVQgSgWAAghQAAgZAJgTQAKgTAQgKQAQgKAYAAQAOAAAMAEQAMAFAIAHQAIAGAGAJQAEAJAAAGQAAAIgFAFQgFAEgHAAQgIAAgEgHQgIgMgIgHQgIgHgMAAQgQAAgKAOQgLAOABAYQAAALACAKQADAIAFAHQAFAHAHADQAGADAJAAQAMAAAGgFQAHgEAGgKIAJgNQADgEAHAAQAHAAAFAGQAEAFAAAGQAAAKgIALQgGALgPAIQgPAJgUAAIgBADQAOABAIAHQAIAHgBAKQABAIgGAHQgEAGgMAEQgKADgOAAQgZAAAAgKg");
	this.shape_3.setTransform(393.4,44.6,1.793,1.793);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAlBlQgGgGAAgKIAAgFQgHAJgGAGQgIAGgIADQgJADgLAAQgOAAgKgGQgLgGgGgLQgHgNAAgXIAAhOQAAgNAFgFQAGgGAHAAQAKAAAFAGQAEAGAAAMIAAA/QAAAMADALQACAKAFAFQAHAFAJAAQAJAAAIgGQAJgGAEgKQACgIABgdIAAgvQgBgMAGgGQAFgGAIAAQAJAAAFAGQAFAGAAAMIAAByQAAAMgFAFQgEAGgJAAQgHAAgFgGgAAJhOQgFgEAAgIQAAgGAFgFQAFgFAGAAQAEAAAEACQADACACAEQADAEAAAEQgBAIgFAEQgEAFgGAAQgGAAgFgFgAgrhOQgFgEAAgIQAAgHAFgFQAEgEAGAAQAGAAAFAEQAFAFAAAHQAAAHgFAFQgFAFgGAAQgGAAgEgFg");
	this.shape_4.setTransform(363.3,34.9,1.793,1.793);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgbBpQgPgIAAgKQAAgMAPAAQAGAAAGADQAIADAEAAQADAAACgCQACgBAAgEQAAgEgCgCQgCgDgEgCIgRgGQgEgCgDgEIgKgDQgVgIAAgQQAAgOAQAAQAGAAANAEQAMAEAGAAQAbAAAAgMQAAgGgTgJQgcgOgHgGQgUgOAAgVQAAgdAdgKQASgGAiAAQANAAAHAEQAJAEAAAOQAAAbgNAAQgOAAgDgMIgLgBQggAAAAAJQAAAGATAJQAaANALAIQAUAPAAATQAAAZgWANIgLAFQAKALAAAOQAAARgKAKQgKAJgSAAQgOAAgMgFg");
	this.shape_5.setTransform(322.8,45.3,1.793,1.793);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgMBnQgGgGAAgMIAAhxQAAgLAGgGQAFgGAHAAQAJAAAFAGQAFAFAAALIAAByQAAAMgFAHQgFAGgJAAQgHAAgFgHgAgMhLQgGgFAAgJQAAgJAGgFQAFgGAHAAQAIAAAFAFQAGAEAAALQAAAJgGAFQgFAFgIAAQgGAAgGgFg");
	this.shape_6.setTransform(303.9,34.5,1.793,1.793);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgtBLQgGgIAAgLIAAhtQAAgbATAAQAJAAAFAGQAEAHAAAMQAGgMAIgHQAGgGAMAAQALAAALAGQAMAHAAAKQAAAHgFAFQgFAFgFgBIgKgCQgIgDgGAAQgJAAgEAFQgFAFgDAJQgDAGgBAPIgBA+QAAANgFAGQgGAGgIAAQgIAAgFgGg");
	this.shape_7.setTransform(287.6,39.6,1.793,1.793);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AArBLQgFgEgHgMQgOALgMAGQgNAFgPAAQgOAAgLgGQgLgGgGgKQgGgLAAgMQAAgPAKgNQAKgLARgDIA/gQQgBgQgFgHQgGgHgQAAQgPAAgGAEQgHAEgFAJIgIALQgBACgIAAQgGAAgFgEQgEgEAAgHQAAgKAHgKQAGgKAQgHQAOgGAWAAQAZAAAOAGQAOAGAGAOQAFAOAAAVIAAAtQAAAMAEALQADALAAAFQAAAGgFAFQgGAFgHAAQgFAAgGgGgAAFAIIgVAFQgHACgFAFQgFAEAAAJQAAAJAGAHQAHAGAKAAQAKAAAKgFQAKgGADgHQAGgKAAgUIAAgGQgGADgSAEg");
	this.shape_8.setTransform(259.8,39.6,1.793,1.793);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgVBoQgFgGAAgMIAAhoIgKAAQgJAAgEgEQgFgEAAgHQAAgOATAAIAJAAIAAgMQAAgSAEgMQAFgLALgFQAKgFASAAQAhAAAAARQAAAFgEAFQgDAEgFAAIgIgBIgJgBQgKAAgDAGQgDAHAAAMIAAAJIAKAAQAVAAAAAOQAAAKgGACQgEADgLAAIgKAAIAABoQAAANgFAFQgFAHgIAAQgIAAgFgHg");
	this.shape_9.setTransform(238.1,34.3,1.793,1.793);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgNBnQgFgFAAgNIAAhxQAAgMAFgFQAGgGAHAAQAIAAAFAGQAGAGAAAKIAAByQAAAMgGAHQgFAGgIAAQgIAAgFgHgAgMhLQgGgEAAgKQAAgJAGgFQAFgGAHAAQAIAAAFAFQAGAFAAAKQAAAIgGAGQgFAFgIAAQgHAAgFgFg");
	this.shape_10.setTransform(221.9,34.5,1.793,1.793);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgwBjQgOgJgJgOQgIgPAAgNQAAgIAFgGQAFgFAIAAQAHAAAEAEQAFAEADAJIAIARQAEAGAJAFQAHAFAOAAQARAAALgJQAMgJAAgOQAAgKgGgHQgGgHgKgDQgKgEgPgDQgYgHgNgGQgPgGgIgNQgIgNAAgSQAAgSAJgNQAJgOARgHQARgIAXAAQATAAANAFQAOAGAIAHQAJAIAEAJQAEAKAAAIQAAAHgFAHQgFAGgIAAQgHAAgDgEQgEgDgEgJQgGgNgIgGQgHgHgQAAQgQAAgKAIQgKAHAAAKQAAAGAEAFQADAFAGADIAMAGIASAFQAVAGAKAEQAMAEAMAIQAKAHAFAKQAFAMAAAQQAAASgJARQgLAQgRAIQgSAJgZAAQgdAAgTgMg");
	this.shape_11.setTransform(198.4,34.3,1.793,1.793);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgkBhQgRgMgJgXQgJgXAAgiQAAgaAGgXQAFgVAKgPQALgPANgGQAPgIARAAQASAAANAIQANAGAHAMQAHALAAAJQAAAGgEAGQgGAEgGAAQgFAAgFgDQgFgEgDgHQgCgJgIgFQgGgFgJgBQgHAAgHAFQgGAFgGAHQgKAPgCAoQAJgLAMgHQAKgFANAAQAOAAALAFQAKAFAIAKQAKAKADALQAFANAAAOQAAAVgJARQgIAPgRALQgPAKgVAAQgVAAgRgNgAgMAAQgIAEgFAKQgFAJABALQAAAVAJANQAKAMANAAQAOAAAKgMQAJgMAAgTQgBgNgEgKQgEgJgIgFQgGgEgKAAQgIAAgHAEg");
	this.shape_12.setTransform(872.2,35.5,1.793,1.793);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgwBsQgKAAgGgHQgGgGAAgIQAAgEAEgKQAFgKADgDIAhgjIAVgTQAKgHAIgJQAGgGAGgLQAEgKAAgIQAAgJgFgIQgDgGgIgFQgHgEgIAAQgSAAgKARIgFAMQgDAKgEAFQgEAFgIAAQgHAAgEgFQgFgFAAgIQAAgLAEgKQAFgMAIgIQAJgJAMgFQAOgGARAAQATAAAQAHQAJAEAIAJQAHAIAEALQAEAMAAAKQAAASgIAPQgIANgKAJIgdAbQgVATgGAJIgHAJIBHAAQAKAAAGAFQAGAEAAAIQAAAIgFAFQgFAFgIAAg");
	this.shape_13.setTransform(842.4,35.2,1.793,1.793);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgkBhQgRgLgJgYQgJgXAAgiQAAgaAFgXQAFgUALgQQAKgOAOgHQAPgIARAAQARAAAOAIQANAGAHAMQAHAKAAAKQAAAGgEAGQgFAEgGAAQgGAAgFgDQgFgFgCgGQgDgJgHgFQgHgFgJgBQgHAAgHAFQgHAFgFAHQgKAQgCAnQAIgKAMgIQALgFANAAQAOAAAKAFQALAFAJAKQAIAJAEAMQAFAMAAAPQAAAVgJARQgIAQgRAKQgPAKgVAAQgWAAgQgNgAgMAAQgIAEgFAKQgFAIAAAMQAAAVAKANQAJAMAOAAQAOAAAJgMQAJgMAAgTQAAgNgEgKQgEgJgIgFQgHgEgJAAQgIAAgHAEg");
	this.shape_14.setTransform(799.5,35.5,1.793,1.793);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgqBjQgPgLgGgMQgHgMAAgKQAAgFAFgFQAEgGAHAAQALAAAGAPQAFAMAKAIQAKAHALAAQAKAAAHgGQAIgFAFgLQAEgIAAgPQAAgOgFgKQgEgKgIgEQgIgEgJgBQgLAAgGADIgOAKQgLAIgFAAQgGAAgGgFQgFgGAAgFIANhQQACgNAFgFQAGgGALAAIBIAAQAVAAAAASQAAAHgFAEQgFAGgKAAIhBAAIgIAuQATgLAQABQANAAAMAFQAMAFAIAJQAJAKAFAMQAFALAAAPQAAAWgJARQgKASgQAKQgRAKgVAAQgYAAgQgJg");
	this.shape_15.setTransform(769.7,35.8,1.793,1.793);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AAVBpQgGgGABgLIAAgbIhFAAQgMAAgHgGQgHgHAAgLIACgFIADgGIBRh0QAGgKAFgFQAEgEAHAAQAVAAAAAZIAABwIAGAAQAKAAAHADQAGAEAAAKQAAAIgFAEQgEAEgLAAIgJAAIAAAbQAAAMgFAFQgFAFgHAAQgIAAgEgFgAgrAcIA7AAIAAhUg");
	this.shape_16.setTransform(726.5,35.4,1.793,1.793);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgwBsQgLAAgFgHQgGgGAAgIQAAgEAEgKQAFgKADgDQASgUAPgPIAVgTQAKgHAIgJQAHgHAEgKQAEgIAAgKQAAgJgEgIQgEgHgHgEQgIgEgHAAQgSAAgKARIgFAMQgDAKgEAFQgEAFgIAAQgHAAgFgFQgEgFAAgIQAAgKAEgLQAFgLAIgJQAJgJAMgFQAOgGARAAQATAAAQAHQAJAEAIAJQAHAIAEALQAEAMAAAKQAAASgIAPQgKAPgIAHIgdAbQgVATgHAJIgGAJIBHAAQALAAAFAFQAGAEAAAIQAAAIgFAFQgFAFgIAAg");
	this.shape_17.setTransform(697,35.2,1.793,1.793);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgwBsQgKAAgGgHQgGgGAAgIQAAgEAEgKQAFgKADgDQASgUAPgPIAVgTQAKgHAIgJQAGgGAGgLQADgJAAgJQAAgKgEgHQgEgHgHgEQgHgEgIAAQgSAAgKARIgFAMQgEALgDAEQgEAFgIAAQgHAAgEgFQgFgFAAgIQAAgKAEgLQAFgLAIgJQAJgJAMgFQAOgGARAAQATAAAQAHQAJAEAIAJQAHAIAEALQAEAMAAAKQAAASgIAPQgIANgKAJIgdAbQgVATgGAJIgHAJIBHAAQALAAAFAFQAGAEAAAIQAAAIgFAFQgFAFgIAAg");
	this.shape_18.setTransform(667,35.2,1.793,1.793);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AghCIIAGgOQAGgNAGgRQAHgUAEgWQAEgYAAgaQAAgZgEgYQgGgZgFgRIgSgsQAAgEAHAAQAJAAAGAEQAEACAJASIAOAcQAHAQADAMQAFARABAMQACANAAARQAAAhgIAbQgHAZgRAfQgJARgFADQgFAEgJAAQgHAAAAgEg");
	this.shape_19.setTransform(629.6,40.6,1.793,1.793);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgqBjQgPgLgGgMQgHgMAAgKQAAgFAFgFQAEgGAHAAQALAAAGAPQAFAMAKAIQAKAHALAAQAKAAAHgGQAIgFAFgLQAEgKAAgNQAAgPgFgJQgEgKgIgEQgIgEgJgBQgMAAgEADIgPAKQgKAIgGAAQgGAAgGgFQgFgGAAgFIANhQQACgMAFgGQAGgGALAAIBIAAQAVAAAAASQAAAHgFAEQgFAGgKAAIhBAAIgIAuQAUgLAQABQAMAAAMAFQANAGAHAIQAJAJAFANQAFANAAANQAAAVgJASQgJASgRAKQgRAKgVAAQgXAAgRgJg");
	this.shape_20.setTransform(606.4,35.8,1.793,1.793);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgqBjQgQgLgGgMQgGgMAAgKQAAgFAFgFQAEgGAHAAQALAAAGAPQAFAMAKAIQAJAHAMAAQAKAAAHgGQAIgFAEgLQAFgKAAgNQAAgOgFgKQgFgKgHgEQgIgEgJgBQgMAAgFADIgOAKQgKAIgGAAQgHAAgFgFQgFgGAAgFIANhQQABgMAGgGQAFgGAMAAIBHAAQAWAAAAASQAAAHgFAEQgFAGgLAAIhAAAIgIAuQATgLAQABQANAAAMAFQANAGAHAIQAJAJAFANQAFAOAAAMQAAAWgJARQgKASgQAKQgQAKgWAAQgXAAgRgJg");
	this.shape_21.setTransform(576.4,35.8,1.793,1.793);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgkBhQgRgNgIgVQgFgQgBgLQgCgNAAgTQAAgXACgQQACgRAFgOQAIgUAPgLQARgLAUAAQAMAAANAEQAMAGAIAJQAHAHAIAQQAKAXAAAtQAAAagCAPQgEASgGAMQgKASgPAKQgPAJgSgBQgUAAgQgMgAgYg8QgIAUAAAoQAAAbADASQADARAIAJQAHAJALAAQANAAAHgJQAHgIADgTQADgRAAgbQAAgbgDgRQgDgQgHgKQgHgIgNAAQgRAAgHASg");
	this.shape_22.setTransform(546,35.4,1.793,1.793);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AARCLQgDgCgFgFQgDgDgHgNQgKgTgIgSQgGgRgEgUQgEgVAAgVQAAgRADgXQAEgUAHgSQAIgSAKgTIAKgRQAGgFADgBQAEgBAGAAQAGAAAAAEIgGAQQgHAPgFAPQgEAMgGAcQgFAYAAAZQAAAbAFAXQAEAXAGASIASAtQAAAEgGAAQgGAAgFgBg");
	this.shape_23.setTransform(522.4,40.6,1.793,1.793);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},146).wait(83));

	// flash0.ai
	this.instance_2 = new lib.Tween16("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(450,-68.3);
	this.instance_2._off = true;

	this.movieClip_1 = new lib.Tween17();
	this.movieClip_1.parent = this;
	this.movieClip_1.setTransform(450,65.5);
	new cjs.ButtonHelper(this.movieClip_1, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_2}]},139).to({state:[{t:this.instance_2}]},9).to({state:[{t:this.movieClip_1}]},5).wait(76));
	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(139).to({_off:false},0).to({y:17.7},9).to({_off:true,y:65.5,mode:"independent"},5).wait(76));

	// Layer 17
	this.instance_3 = new lib.Tween1("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(1091.4,50);

	this.instance_4 = new lib.Tween2("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(708.4,50);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_3}]}).to({state:[{t:this.instance_4}]},27).to({state:[{t:this.instance_4}]},96).to({state:[{t:this.instance_4}]},14).to({state:[]},18).wait(74));
	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({_off:true,x:708.4},27).wait(202));

	// Layer 20
	this.instance_5 = new lib.Tween12("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(363.6,304);

	this.instance_6 = new lib.Tween13("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(363.6,-203.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_5}]}).to({state:[{t:this.instance_6}]},136).to({state:[]},19).wait(74));
	this.timeline.addTween(cjs.Tween.get(this.instance_5).to({_off:true,y:-203.9},136).wait(93));

	// Layer 15
	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AAQAlIgEghIAAgNIABgDIAAgEQAAgHgDAAQgGAAgGAJQgFAJgEANIgDAcQAAAFgDADQgCADgFAAQgEAAgEgDQgCgDgBgFIAAgOIAAgWIABgUIAAgPQAAgFADgDQADgDAEAAQALAAAAANIAAABQALgMALAAQAQAAAEANQAEAIAAARIAAAKIACAQQACAKgBAHQAAAFgCACQgEADgEAAQgKAAAAgKg");
	this.shape_24.setTransform(200.5,18.9,1.09,1.089);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AAPA3QgJAEgKAAQgLAAgIgFQgIgGgBgKQgDgUAAgQQAAgLACgLQABgKAJABQAFgBADADQADADAAAFIgBAKIgBALIAAATIACAOIAEACIAEAAQAGAAALgCIAAgcQAAgPABgMQACgJAJAAQAEgBADADQADADAAAFIgBBGQAAAEgDADQgDADgEAAQgGAAgDgGgAAMgoQgEgEAAgEQAAgMAPAAQAEAAAFAEQAEADAAAEQAAAFgFAEQgFACgFAAQgEAAgFgCgAgZgxQAAgFAFgCQAEgEAEAAQAOAAAAAMQAAAEgEADQgFADgEAAQgOAAAAgLg");
	this.shape_25.setTransform(192,17.5,1.09,1.089);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgOA5QgIgEAAgHQAAgHAIAAIAHACQADACADAAIADgBIABgDQAAgCgDgDQgBgCgFgCQgMgDgJgKQgIgJAAgOQAAgQALgSQAMgUAPAAQAIAAAKAFQANAGAAAHQAAADgCAEQgDADgEAAQgEAAgEgDQgEgEgKAAQgFAAgHAMQgGAMAAAJQAAAHAGAFQAFAFAIAAQAEAAAIgEQAIgFACAAQADAAAEAEQADADAAAEQAAAEgGAFQgFADgHADIACAFIABAGQAAAKgGAGQgFAGgKAAQgFAAgJgEg");
	this.shape_26.setTransform(182.9,20.7,1.09,1.089);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AAPA3QgJAEgKAAQgLAAgHgFQgJgGgBgKQgDgUAAgQQAAgLACgLQABgKAJABQAFgBADADQADADAAAFIgBAKIgBALIAAATIACAOIAEACIAEAAQAGAAALgCIAAgcQAAgPABgMQACgJAJAAQAEgBADADQADADAAAFIgBBGQAAAEgDADQgDADgEAAQgGAAgDgGgAANgoQgFgEAAgEQAAgMAQAAQAEAAAEAEQAEADAAAEQAAAFgFAEQgFACgFAAQgEAAgEgCgAgZgxQAAgFAFgCQAEgEAEAAQAPAAgBAMQAAAEgEADQgFADgEAAQgOAAAAgLg");
	this.shape_27.setTransform(174.5,17.5,1.09,1.089);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgOA5QgJgDAAgHQAAgGAJAAIAGACQAEACADAAIACgBIABgDIgBgEIgDgCIgJgEIgEgDIgGgBQgLgFAAgJQAAgIAJAAIAKADIAKACQAPAAAAgHQAAgDgLgFQgPgHgEgEQgLgHAAgMQAAgPARgGQAKgEARAAQAIAAAEACQAFADAAAIQAAAOgIAAQgHAAgCgHIgGAAQgRAAAAAFQAAADAKAGQANAGAHAFQALAIAAAKQAAAPgMAGIgGADQAFAGAAAHQAAAJgFAGQgGAGgJAAQgGAAgIgEg");
	this.shape_28.setTransform(158.1,20.7,1.09,1.089);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgJA7QgDgEAAgEIABhAQAAgFADgDQADgEAEAAQAEAAADAEQADADAAAFIgBBAQAAAEgDAEQgDACgEABQgEgBgDgCgAgGgoQgEgEAAgEQAAgGAEgDQADgEAEAAQAFAAADAEQAEAEAAAFQAAAEgEAEQgEADgEAAQgDAAgEgDg");
	this.shape_29.setTransform(152,17,1.09,1.089);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgdApQgCgDgBgDIABhEQAAgEADgDQACgDAEAAQAJAAABAJQANgKARAAQAOAAAAAVIAAAGQgBAMgJAAQgKAAAAgKIAAgJQgQACgIARIAAAoQAAAEgCADQgDADgFAAQgFAAgCgEg");
	this.shape_30.setTransform(145.5,19.1,1.09,1.089);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AASAlIgMAFQgFACgDAAQgSAAgKgKQgIgLgBgUQAAgTAOgNQAOgOARAAQAIAAAIAEQAMAFAAAHQAAADgCACIgBATQAAAPACAGIAGAQQAAAFgCACQgEADgEAAQgDAAgIgHgAgKgPQgHAIAAAKQgBALAEAGQAEAFAHAAQADAAAEgCIAIgEQgCgSAAgKIAAgGIABgHIgCgCIgCAAQgKAAgHAJg");
	this.shape_31.setTransform(136.6,19.1,1.09,1.089);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgOBEQgDgDAAgFIAAgCQACgKAAgLIAAgnIgJABQgFAAgDgDQgDgDAAgFQAAgHAHgDIAOgBIABgJQACgTAHgIQAIgLASAAQAOAAAAALQAAAKgNAAQgKAAgDAHQgDAEgCAMIAAACIAPgBQAOAAAAALQAAAKgPAAIgPAAIAAArQAAAOgBAJQgCAJgHAAQgFAAgDgDg");
	this.shape_32.setTransform(127.5,17.1,1.09,1.089);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgJA7QgDgEAAgEIABhAQAAgFADgDQADgEAEAAQAEAAADAEQADADAAAFIgBBAQAAAEgDAEQgDACgEABQgEgBgDgCgAgGgoQgEgEAAgEQAAgFAEgEQADgEAEAAQAFAAAEAEQADADAAAGQAAAEgDAEQgFADgEAAQgDAAgEgDg");
	this.shape_33.setTransform(121.1,17,1.09,1.089);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgoA1QgJgJAAgJQAAgGACgCQAEgEAEAAQAHAAADAHQAFAKAPAAQAMAAAMgFQANgHAAgHQAAgLgIgCQgFgCgRgCQgNAAgKgEQgOgGAAgMQAAgQAQgNQAQgNASgBQAGABAMADQANAEAAAHQAAAEgCADQgDADgFABIgVgDQgLAAgIAEQgIAFgBAGQABADADACQACADAGAAIASABQARABAKAJQALAJgBARQABAUgXAMQgRAHgVABQgSgBgMgIg");
	this.shape_34.setTransform(112.7,17.3,1.09,1.089);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AInFtQAAAAAAgBQAAgBABAAQAAgBABAAQAAAAABAAIAkAAQABAAABAAQAAAAABABQAAAAAAABQAAAAAAABQACAEgFAAIgkAAQgBAAAAAAQgBAAAAgBQgBAAAAgBQAAgBAAgBgAH0FxQgBAAgBAAQAAAAgBgBQAAAAAAgBQAAgBAAgBQAAAAAAgBQAAgBAAAAQABgBAAAAQABAAABAAIAkAAQABAAABAAQAAAAABABQAAAAAAABQAAABAAAAQAAABAAABQAAABAAAAQgBABAAAAQgBAAgBAAgAG9FxQAAAAgBAAQgBAAAAgBQgBAAAAgBQAAgBAAgBQAAAAAAgBQAAgBABAAQAAgBABAAQABAAAAAAIAlAAQABAAAAAAQABAAAAABQABAAAAABQAAABAAAAQAAABAAABQAAABgBAAQAAABgBAAQAAAAgBAAgAGIFxQgBAAgBAAQgBAAAAgBQgBAAAAgBQAAgBAAgBQAAAAAAgBQAAgBABAAQAAgBABAAQABAAABAAIAjAAQABAAABAAQABAAAAABQABAAAAABQAAABAAAAQAAABAAABQAAABgBAAQAAABgBAAQgBAAgBAAgAFRFxQgBAAgBAAQAAAAgBgBQAAAAAAgBQAAgBAAgBQAAAAAAgBQAAgBAAAAQABgBAAAAQABAAABAAIAkAAQABAAABAAQAAAAABABQAAAAAAABQAAABAAAAQAAABAAABQAAABAAAAQgBABAAAAQgBAAgBAAgAJdFtQAAgBAAgBQAAAAABgBQAAAAABAAQABAAAAAAIAkAAQABAAABAAQABAAAAAAQABABAAAAQAAABAAAAQAAABAAABQAAABgBAAQAAABgBAAQgBAAgBAAIgkABQAAAAgBgBQgBAAAAAAQgBgBAAAAQAAgBAAgBgAEbFxQgBAAgBgBQAAAAgBAAQAAgBAAAAQAAgBAAgBQAAgBAAgBQAAAAAAgBQABAAAAAAQABAAABAAIAkAAQABAAABAAQAAAAABABQAAAAAAABQAAABAAAAQAAABAAABQAAAAAAABQgBAAAAAAQgBABgBAAgADlFxQgBAAgBgBQgBAAAAAAQgBgBAAAAQAAgBAAgBQAAgBAAgBQAAAAABgBQAAAAABAAQABAAABAAIAkAAQAAAAABAAQABAAAAAAQABABAAAAQAAABAAABQAAABAAABQAAAAgBABQAAAAgBAAQgBABAAAAgAKUFsQAAAAAAgBQAAgBAAAAQABgBAAAAQABAAAAAAIAlAAQABAAABAAQAAAAABABQAAAAAAABQABAAAAABQAAABgBABQAAAAAAABQgBAAAAAAQgBAAgBAAIglABQAAAAgBAAQAAAAgBgBQAAAAAAgBQAAgBAAgBgACuFwQgBAAgBAAQAAAAgBgBQAAAAAAgBQgBgBAAgBQAAAAABgBQAAgBAAAAQABgBAAAAQABAAABAAIAlABQAAAAABAAQAAAAABAAQAAABAAAAQAAABAAABQAAABAAAAQAAABAAAAQgBABAAAAQgBAAAAAAgAB4FvQgBAAgBAAQAAAAgBAAQAAgBAAAAQgBgBAAgBQAAgBABAAQAAgBAAAAQABgBAAAAQABAAABAAIAkAAQABAAABAAQAAAAABABQAAAAAAABQAAABAAAAQAAABAAABQAAABAAAAQgBABAAAAQgBAAgBAAgALKFsQAAgBAAgBQAAAAAAgBQABAAAAAAQABAAABAAIAkgBQABAAABAAQAAAAABAAQAAABAAAAQABABAAABQAAABgBABQAAAAAAABQgBAAAAAAQgBAAgBAAIgkABQgBAAgBAAQAAAAgBAAQAAgBAAAAQAAgBAAgBgABCFvQgBAAgBAAQgBAAAAgBQgBAAAAgBQAAAAAAgBQAAgBAAgBQAAAAABgBQAAAAABAAQABAAABAAIAkAAQAAAAABAAQABAAAAAAQABABAAAAQAAABAAABQAAABAAABQAAAAgBABQAAAAgBAAQgBAAAAAAgAAPFvQgBAAgBAAQAAAAgBgBQAAAAAAgBQAAAAAAgBQAAgBAAgBQAAAAAAgBQABAAAAAAQABAAABAAIAkAAQABAAABAAQAAAAABAAQAAABAAAAQABABAAABQAAABgBABQAAAAAAABQgBAAAAAAQgBAAgBAAgAgnFvQgBAAgBAAQgBAAAAgBQgBAAAAgBQAAAAAAgBQAAgBAAgBQAAAAABgBQAAAAABAAQABAAABAAIAkAAQABAAAAAAQABAAAAAAQABABAAAAQAAABAAABQAAABAAABQAAAAgBABQAAAAgBAAQAAAAgBAAgAhcFvQgBAAgBAAQAAAAgBgBQAAAAAAgBQAAgBAAgBQAAAAAAgBQAAgBAAAAQABgBAAAAQABAAABAAIAkAAQABAAABAAQAAAAABABQAAAAAAABQABABAAAAQAAABgBABQAAABAAAAQgBABAAAAQgBAAgBAAgAiSFuQgBAAgBAAQAAAAgBAAQAAgBAAAAQAAgBAAgBQAAgBAAgBQAAAAAAgBQABAAAAAAQABAAABAAIAkAAQABAAABAAQAAAAABABQAAAAAAABQABAAAAABQAAABgBABQAAAAAAABQgBAAAAAAQgBAAgBAAgAMAFqQAAAAAAgBQAAgBABAAQAAgBABAAQAAAAABAAIAkgBQABAAABAAQAAAAABAAQAAABAAAAQABABAAABQAAABAAABQAAAAgBABQAAAAgBAAQgBAAgBAAIgkACQAAAAgBAAQgBAAAAgBQgBAAAAgBQAAgBAAgBgAjJFuQAAAAgBAAQgBAAAAgBQgBAAAAgBQAAgBAAgBQAAAAAAgBQAAgBABAAQAAgBABAAQABAAAAAAIAlAAQABAAAAABQABAAAAAAQABABAAAAQAAABAAAAQAAABAAABQAAABgBAAQAAABgBAAQgBAAgBAAgAj/FtQgBAAAAAAQgBAAAAgBQgBAAAAgBQAAgBAAAAQAAgBAAgBQAAAAABgBQAAAAABAAQAAgBABAAIAkAAQABAAABABQABAAAAAAQABABAAAAQAAABAAABQAAABAAABQAAAAgBABQAAAAgBAAQgBAAgBAAgAk1FsQgBAAgBAAQAAAAgBAAQAAgBAAAAQAAgBAAgBQAAgBAAgBQAAAAAAgBQABAAAAAAQABgBABAAIAkABQABAAABAAQAAAAABABQAAAAAAABQAAABAAABQAAAAAAABQAAABAAAAQgBABAAAAQgBAAgBAAgAlsFsQAAAAgBAAQgBAAAAgBQgBAAAAgBQAAgBAAAAQAAgBAAgBQAAgBABAAQAAgBABAAQABAAAAAAIAlAAQABAAAAABQABAAAAAAQABABAAAAQAAABAAABQAAABAAABQAAAAgBABQAAAAgBAAQAAAAgBAAgAmiFrQgBAAAAAAQgBAAAAAAQgBgBAAAAQAAgBAAgBQAAgBAAgBQAAAAABgBQAAAAABAAQABgBABAAIAjABQABAAABAAQABAAAAABQABAAAAABQAAABAAABQAAAAAAABQAAABgBAAQAAABgBAAQgBAAgBAAgAM2FpQAAgBAAgBQAAgBABAAQAAgBABAAQABAAAAAAIAkgEQADABABADQAAABAAABQAAAAgBABQAAAAgBAAQAAABgBAAIglACQAAAAgBAAQgBAAAAAAQgBgBAAAAQAAgBAAAAgAnYFqQgBAAgBAAQAAAAgBAAQAAgBAAAAQAAgBAAgBQAAgBAAgBQAAAAAAgBQABAAAAAAQABgBABAAIAkABQABAAABAAQAAAAABABQAAAAAAABQAAABAAABQAAAAAAABQAAABAAAAQgBABAAAAQgBAAgBAAgAoOFqQgBAAgBAAQgBAAAAgBQgBAAAAgBQAAgBAAgBQAAAAABgBQAAgBAAAAQABgBAAAAQABAAABAAIAkAAQABAAABABQAAAAABAAQAAABAAAAQAAABAAABQAAABAAABQAAAAgBABQAAAAgBAAQAAAAgBAAgApFFpQgBAAAAAAQgBAAAAgBQgBAAAAgBQAAgBAAgBQAAAAAAgBQAAAAABgBQAAAAABAAQABgBABAAIAkAAQAAAAABABQABAAAAAAQABABAAAAQAAABAAABQAAABAAABQAAAAgBABQAAAAgBAAQgBABAAAAgAp7FoQgBAAgBAAQAAAAgBgBQAAAAAAgBQgBgBAAgBQAAAAABgBQAAAAAAgBQABAAAAAAQABgBABAAIAkABQABAAABAAQAAAAABABQAAAAAAABQAAABAAAAQAAABAAABQAAABAAAAQgBABAAAAQgBAAgBAAgAqxFoQgBgBgBAAQgBAAAAgBQgBAAAAgBQAAAAAAgBQAAgBAAgBQAAAAABgBQAAAAABAAQABAAABAAIAkAAQAEAAgBADQAAABAAABQAAABgBAAQAAABgBAAQAAAAgBAAgAroFmQgEAAABgEQAAAAAAgBQAAAAABgBQAAAAABAAQAAgBABAAIAlABQAAAAABAAQABAAAAABQABAAAAABQAAABAAAAQAAABAAABQAAABgBAAQAAABgBAAQgBAAAAAAgANsFjQAAgDADAAQAXgFALgEQABgBABAAQABAAAAABQABAAAAAAQAAABAAAAQABABAAABQAAAAAAABQAAAAgBABQAAABgBAAQgPAGgVADIgBAAQAAAAgBAAQAAAAgBAAQAAgBAAAAQgBgBAAgBgAseFkQgBAAgBAAQAAgBgBAAQAAgBAAAAQgBgBAAAAQAAgFAEACIAkABQABAAABAAQAAAAABAAQAAABAAAAQAAABAAABQAAABAAABQAAAAAAABQgBAAAAAAQgBAAgBAAgAtVFgQgBAAAAAAQgBgBAAAAQgBgBAAAAQAAgBAAgBQAAgBABAAQAAgBAAAAQABgBAAAAQABAAABAAIAkADQABAAAAABQABAAAAAAQABABAAAAQAAABAAABQAAABAAABQAAAAgBABQAAAAgBAAQgBAAAAAAgAtnFdQgYgFgLgHQgBAAAAAAQgBgBAAAAQAAgBAAAAQAAgBABgBIACgBIACAAQALAFAXAGQAAAAABAAQABAAAAABQAAAAAAABQAAAAAAABQAAABAAABQAAAAgBAAQAAABgBAAQAAAAgBAAgAOgFSQgBAAAAgBQgBAAAAgBQAAAAABgBQAAAAABgBQANgKAFgTQAAgBABAAQAAgBABAAQAAAAABAAQABAAAAAAQABAAABABQAAAAAAABQABAAAAABQAAABgBABQgGATgOAMIgCABQAAAAgBAAQAAAAAAAAQgBgBAAAAQAAAAAAgBgAuaFHQgNgNgIgSQgBgBAAgBQAAAAABgBQAAAAAAgBQABAAABgBQAAAAABAAQABAAAAAAQABAAAAABQABAAAAABQAJATALAJQACADgCADIgDAAgAO5EkQgDgBAAgDQACgNABgVIABgCQAAgBAAgBQAAAAABgBQAAAAABAAQABgBABAAQAAAAABABQAAAAABAAQAAABAAAAQAAABAAABIAAACQAAAQgDATQAAABgBAAQAAABAAAAQgBABAAAAQgBAAgBAAgAu2EXQgGgRgDgTQAAgBAAgBQAAgBAAgBQAAAAABAAQABgBAAAAQABAAABAAQAAABABAAQAAAAABABQAAAAABABQAEAXAEAMQAAABABABQAAABgBAAQAAABAAAAQgBAAgBABIgBAAQgBAAAAAAQgBAAAAgBQgBAAAAAAQAAgBAAAAgAO7DqIAAglQAAgBAAgBQAAAAAAgBQABAAAAAAQABgBABAAQABAAABABQAAAAABAAQAAABAAAAQAAABAAABIAAAlQAAAAAAABQAAABgBAAQAAABgBAAQgBAAAAAAQgBAAgBAAQAAAAgBgBQAAAAAAgBQAAgBAAAAgAu+DjQgBAAgBAAQAAAAgBgBQAAAAAAgBQAAAAAAgBQgDgSgBgTQAAgBABgBQAAAAAAgBQABAAAAAAQABgBABAAQABAAAAABQABAAAAAAQABABAAAAQAAABABABIACAkQAAABAAABQAAAAgBABQAAAAgBABQAAAAgBAAgAO7CyIAAgkIAGAAIAAAkQAAABAAABQAAABAAAAQgBABAAAAQgBAAgBAAQgBAAAAAAQgBAAAAgBQgBAAAAgBQAAgBAAgBgAvFCoIAAgkQAAgBAAgBQAAAAABgBQAAAAABAAQAAgBABAAQABAAABABQAAAAABAAQAAABAAAAQABABAAABIAAAkQAAABAAABQAAABgBAAQAAABgBAAQgBAAAAAAQgBAAgBAAQgBAAAAgBQgBAAAAgBQAAgBAAgBgAO7B7IAAglQAAAAAAgBQAAgBAAAAQABgBAAAAQABAAABAAQABAAABAAQAAAAABABQAAAAAAABQAAABAAAAIAAAlQAAABAAABQAAABAAAAQgBABAAAAQgBAAgBAAQgBAAAAAAQgBAAAAgBQgBAAAAgBQAAgBAAgBgAvFBxIAAgkQAAgBAAgBQAAAAABgBQAAAAABAAQABAAABAAQAAAAABAAQAAAAABAAQAAABAAAAQABABAAABIAAAkQAAABgBABQAAABAAAAQgBABAAAAQgBAAgBAAQgBAAAAAAQgBAAAAgBQgBAAAAgBQAAgBAAgBgAO7BEIAAglQAAAAAAgBQAAgBAAAAQABgBAAAAQABAAABAAQAAAAABAAQABAAAAABQABAAAAABQAAABAAAAIAAAlQAAABAAABQAAAAAAABQgBAAAAAAQgBABgBAAQgBAAgBgBQAAAAgBAAQAAgBAAAAQAAgBAAgBgAvFA6IAAglQAAAAAAgBQAAgBABAAQAAgBABAAQABAAABAAQAAAAABAAQABAAAAABQABAAAAABQAAABAAAAIAAAlQAAABAAABQAAABgBAAQAAABgBAAQgBAAAAAAQgBAAgBAAQgBAAAAgBQgBAAAAgBQAAgBAAgBgAO6ANIAAgkQAAAAAAgBQAAgBABAAQAAgBABAAQAAAAABAAQABAAABAAQAAAAABABQAAAAAAABQAAABAAAAIABAkQAAABAAABQAAAAgBABQAAAAgBAAQgBABAAAAQgBAAgBgBQgBAAAAAAQgBgBAAAAQAAgBAAgBgAvFADIAAgjQAAgBABgBQAAgBAAAAQABgBAAAAQABAAABAAQABAAABAAQAAAAABABQAAAAAAABQABABAAABIgBAjQAAABAAABQAAAAgBABQAAAAgBAAQgBAAAAAAQgBAAgBAAQAAAAgBAAQAAgBAAAAQgBgBAAgBgAO4guIACglQAAAAAAgBQAAgBABAAQAAgBABAAQABAAABAAQADABAAADQgBASgBASQAAABAAABQAAABAAAAQgBABAAAAQgBAAgBAAQgBAAgBgBQAAAAgBAAQAAgBAAAAQgBgBAAgBgAvCg4IABglQAAAAAAgBQAAgBAAAAQABgBAAAAQABAAABAAQABAAAAAAQABAAAAABQABAAAAABQAAABAAAAIAAAlQAAABAAABQAAABgBAAQAAABgBAAQgBAAAAAAQgBAAgBgBQgBAAAAAAQgBgBAAAAQAAgBAAgBgAO7hlIADglQAAgDADAAQABAAAAABQABAAAAAAQABABAAAAQAAABAAABIgCAkQAAABAAABQAAABgBAAQAAABgBAAQgBAAgBAAQgDgBAAgDgAvBhvIAAglQAAAAAAgBQAAgBABAAQAAgBABAAQABAAABAAQAAAAABAAQABAAAAABQABAAAAABQAAABAAAAIgBAlQAAABAAABQAAAAAAABQgBAAAAAAQgBABgBAAQgBAAgBgBQAAAAgBAAQAAgBAAAAQAAgBAAgBgAO+icIABglQAAAAAAgBQAAgBABAAQAAgBABAAQABAAAAAAQABAAABAAQABAAAAABQABAAAAABQAAABAAAAIgBAlQAAABAAABQAAABgBAAQAAABgBAAQgBAAgBAAQgDgBAAgDgAvBimIABglQAAgBAAAAQAAgBAAAAQABgBAAAAQABAAABAAQABAAABAAQAAAAABABQAAAAAAABQAAABAAAAIAAAlQAAABAAABQAAAAgBABQAAAAgBAAQgBABAAAAQgBAAgBgBQgBAAAAAAQgBgBAAAAQAAgBAAgBgAO/jTIgCgkQAAgDACgBQADAAACADIACAlQAAABgBABQAAAAAAABQgBAAAAAAQgBABgBAAQAAAAgBgBQgBAAAAAAQgBgBAAAAQAAgBAAgBgAvAjdIAAglQAAgBAAgBQAAAAABgBQAAAAABAAQABgBAAAAQABAAABABQABAAAAAAQABABAAAAQAAABAAABIgBAlQAAABAAABQAAAAAAABQgBAAAAAAQgBAAgBAAQgBAAAAAAQgBAAAAAAQgBgBAAAAQAAgBAAgBgAO+kGQgBAAgBAAQAAAAgBgBQAAAAAAgBQAAAAAAgBQgDgPgFgVQgBAAAAgBQAAgBABAAQAAAAAAgBQABAAABAAQAAgBABAAQABAAAAABQABAAAAAAQABABAAABQAFAPADAVQAAABAAAAQAAABAAABQgBAAAAAAQgBABgBAAgAvAkUIAAgFQAAgSAJgOQAAgBABgBQAAAAABAAQAAAAABAAQAAAAABABQABAAAAABQABAAAAABQAAAAAAABQAAAAgBABQgHANAAAQIAAAFQAAAAAAABQAAABgBAAQAAABgBAAQgBAAgBAAQAAAAgBAAQgBAAAAgBQgBAAAAgBQAAgBAAAAgAurlDQgDgCADgDQAKgJAWgKQABAAAAAAQABgBAAABQABAAAAAAQABABAAAAQABABAAABQAAABgBABQAAAAAAABQgBAAAAAAQgXALgHAIIgDAAgAt5lcQgBAAgBAAQAAAAgBgBQAAAAAAAAQAAgBgBAAQAAgBAAgBQAAAAAAgBQABAAAAgBQABAAAAgBIAIgDQAGgDAWgBQAEgBAAAEQABADgEAAQgVACgFACIgIAEgAtIlnQAAgEADgBIAlgBQADgBABAEQAAABgBABQAAABAAAAQgBABAAAAQgBAAgBAAIgkABQgBAAgBAAQAAAAgBAAQAAgBAAAAQgBgBAAAAgAsRlqQgBgDAEAAIAlgBQAAAAABAAQAAAAABABQAAAAAAABQABABAAAAQAAAEgDAAIglABQgBAAAAgBQgBAAAAAAQgBgBAAAAQAAgBAAgBgArblrQAAgBAAgBQAAAAABgBQAAAAABAAQABAAABAAIAkgBQAAAAABAAQABAAAAABQABAAAAABQAAABAAAAQAAABAAABQAAABgBAAQAAABgBAAQgBAAAAAAIgkABQgBAAgBgBQgBAAAAAAQgBgBAAAAQAAgBAAgBgAqllsQAAAAABgBQAAgBAAAAQABgBAAAAQABAAABAAIAkAAQABAAABAAQAAAAABABQAAAAAAABQAAAAAAABQAAABAAABQAAAAAAABQgBAAAAAAQgBAAgBAAIgkABQgBAAgBAAQAAAAgBgBQAAAAAAgBQgBgBAAgBgAprlpQgBAAgBAAQAAAAgBAAQAAgBAAAAQgBgBAAgBQAAgBABgBQAAAAAAgBQABAAAAAAQABAAABAAIAkAAQABAAABAAQAAAAABAAQAAABAAAAQAAABABABQgBABAAABQAAAAAAABQgBAAAAAAQgBAAgBAAgAnIlpQgBAAgBAAQAAAAgBgBQAAAAAAgBQgBgBAAgBQAAAAABgBQAAgBAAAAQABgBAAAAQABAAABAAIAkABQABAAABAAQAAAAABAAQAAABAAAAQAAABAAAAQAAABAAABQAAABAAAAQgBABAAAAQgBAAgBAAgAn+lpQgBAAgBAAQgBAAAAgBQgBAAAAgBQAAgBAAgBQAAAAAAgBQAAgBABAAQAAgBABAAQABAAABAAIAkAAQABAAAAAAQABAAAAABQABAAAAABQAAABAAAAQAAABAAABQAAABgBAAQAAABgBAAQAAAAgBAAgAo0lpQgBAAgBAAQgBAAAAgBQgBAAAAgBQAAAAAAgBQAAgBAAgBQAAAAABgBQAAAAABAAQABAAABAAIAkAAQAAAAABAAQABAAAAAAQABABAAAAQAAABAAAAQAAABAAABQAAABgBAAQAAABgBAAQgBAAAAAAg");
	this.shape_35.setTransform(110.8,56.8,1.09,1.089);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AhWAzQgPgZAAgcQAAgqAegfQAfgeAqAAQArAAAeAeQAfAfAAAqQAAAqgfAeQgeAfgrAAQgbAAgWgNIg6ASgAACBUQAjAAAagaQAZgZAAgjQAAgjgZgaQgagZgjAAQgjAAgZAZQgaAZAAAkQAAAaARAYIgLAgIAhgLQAVAOAaABgAADApQgPgHgPgSIgMgQQgDgCgDgIQgFgJABgJQAAgLAKgKQADgEAEAAIAFABQAFAAABAEIAIAUQAAABAAABQAAABAAAAQAAABAAAAQAAABAAAAIgDAEIgEAFQgDACABADQAIAMAGAGQAGAGAHAEIAHAEQAEACACgDIAJgJQACgDAEACIARAJIAFADQACADgEAJQgBAFgHAEQgGADgEABIgHABQgHgCgSgHg");
	this.shape_36.setTransform(58.4,43.8,1.09,1.089);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("Ag3BbQgMgHgKgMQgLgNgBgLQgBgQANgIQAPgKAZAOIgFALQgTgLgKAGQgHAFABAIQABAHAIALQAJAKAKAFQAXAPAbAAQAogBAcgcQAcgdAAgnQgBgngcgbQgcgagoAAQgoABgcAeQgcAeAAAoIgMAAQAAgxAgggQAegfAuAAQAsgBAgAfQAgAfABAsQAAAsggAfQgfAfgtABQgeAAgagQg");
	this.shape_37.setTransform(29.6,43.8,1.09,1.089);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AgyBBQgEgGAEgNQAJgRALgBQALgCAHAJQAIgIAGgNIAJgSQAEgOAAgLQgLABgGgJQgHgKAHgSQAEgMAIgBIADAAQAIAAAIADQALAFAHALQAHANgBAUQAAAUgHASIgGAQQgJATgOANQgPAOgMAEIgKABQgSAAgMgOgAgnAzIgCAHIAGAFQAHAFAIAAIAHgBQALgDALgNQAMgMAIgRIAGgOQAHgQAAgRQAAgRgGgLQgIgPgRACIgDAGQgFALAEAFQADAFAHgDIAHgEIABAIQAEAOgHAYIgBAAIgIATQgMAWgMAIIgGAEIgCgIQgCgGgGAAIgBAAQgFABgGALg");
	this.shape_38.setTransform(29.6,43.5,1.09,1.089);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AguA9QgCgDAEgKQAGgNAJgCQAFgBAFADQAEADABAFQAMgHAKgUIAIgSQAHgWgDgNQgFACgFgBQgFgBgDgEQgFgIAFgOQAEgJAEAAQAHgBAIACQALAEAGALQAGALAAASQAAATgHASIgGAOQgJATgNANQgNAMgMAEIgJABQgOAAgLgMg");
	this.shape_39.setTransform(29.6,43.5,1.09,1.089);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AgUA2QgJgHgFgNQgFgMAAgTQAAgPADgMQADgNAGgGQAFgIAIgEQAHgEAKAAQALAAAGAEQAHADAEAHQAEAFAAAGQAAAEgCACQgDADgDAAQgEAAgCgCQgDgCgBgEQgCgFgEgDQgDgDgGAAQgCAAgFACQgEADgCAEQgGAJgBAVQAGgHAFgCQAGgDAHAAQAHAAAGADQAGACAFAGQAEAEADAHQADAIAAAHQAAAKgFALQgGAJgIAFQgIAFgMAAQgMAAgJgGgAgGAAQgFACgCAFQgDAFAAAHQAAALAFAHQAGAHAHAAQAHAAAFgGQAFgHAAgLQAAgIgCgEQgCgFgEgDQgFgCgEAAQgEAAgEACg");
	this.shape_40.setTransform(199.9,44.1,1.09,1.089);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AgaA7QgFAAgDgDQgEgDAAgFIACgIIAEgHIAogmQAFgGABgEQACgFAAgEQAAgFgCgEQgCgFgFgCQgEgCgDAAQgKAAgFAJIgDAHIgEAIQgCADgFAAQgEAAgCgDQgDgCAAgFQAAgFADgGQADgHAEgEQAEgFAIgDQAHgDAJAAQAMAAAHADQAFADAFAFQAEAFACAFQACAFAAAHQAAAKgFAIQgEAHgFAFIgRAPQgKAJgEAGIgEAFIAnAAQAFAAAEADQADADAAAEQAAADgCAEQgDACgFAAg");
	this.shape_41.setTransform(189.9,43.9,1.09,1.089);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AgUA2QgIgHgGgNQgFgOAAgRQAAgPADgMQADgLAGgIQAGgIAHgEQAIgEAJAAQALAAAGAEQAHADAEAHQAEAGAAAFQAAADgCADQgDADgDAAQgEAAgCgCQgDgCgBgEQgCgFgEgDQgDgDgFAAQgDAAgEACIgHAHQgFAIgCAWQAGgGAGgDQAGgDAGAAQAHAAAHADQAFACAFAGQAFAGACAFQADAIAAAHQAAALgFAKQgFAJgJAFQgIAFgMAAQgLAAgKgGgAgGAAQgEACgDAFQgDAGAAAGQAAAKAGAIQAFAHAHAAQAIAAAFgGQAFgIAAgKQAAgGgDgGQgCgEgEgEQgEgCgFAAQgEAAgEACg");
	this.shape_42.setTransform(175.5,44.1,1.09,1.089);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AgWA2QgIgFgFgHQgDgIAAgEQAAgDACgDQADgDAEAAQAGAAADAIQADAHAGAEQAEAEAHAAQAEAAAFgDQAFgDACgGQACgGAAgHQAAgHgCgGQgEgGgDgBQgFgDgEAAQgFAAgEACIgIAFQgGAEgDAAQgEAAgCgDQgEgCAAgDIAIgsQABgGADgFQADgDAGAAIAnAAQAMAAAAAKQAAAEgDADQgDADgFgBIgkAAIgDAaQAKgGAIAAQAHAAAHAEQAGACAFAGQAEAEADAGQADAGAAAKQAAAMgFAIQgEAKgKAGQgKAGgLgBQgMABgJgGg");
	this.shape_43.setTransform(165.5,44.1,1.09,1.089);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AALA6QgDgDAAgGIAAgPIgkAAQgIAAgDgEQgEgEAAgFIABgDQAAgBAAAAQAAgBAAAAQABgBAAAAQAAAAAAgBIADgDIACgEIAuhAQADgDADAAQALAAAAAOIAAA+IAEAAQAGAAADABQAEACAAAGQAAAFgDABQgEADgFAAIgFAAIAAAPQABAGgDADQgCADgFAAQgEAAgDgDgAgXAQIAfAAIAAgug");
	this.shape_44.setTransform(151.1,44,1.09,1.089);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AgaA7QgFAAgDgDQgEgDAAgFIACgIIAFgHIAngmQAEgGACgEQACgFAAgEQAAgFgCgEQgCgFgFgCQgEgCgDAAQgKAAgFAJIgDAHIgEAIQgCADgFAAQgEAAgCgDQgCgCgBgFQABgFACgGQACgHAFgEQAEgFAIgDQAHgDAJAAQAMAAAHADQAGADAEAFQAEAFACAFQACAFAAAHQAAAKgFAIQgDAHgGAFIgRAPQgKAJgEAGIgEAFIAnAAQAFAAAEADQADADAAAEQAAADgCAEQgDACgFAAg");
	this.shape_45.setTransform(141.2,43.9,1.09,1.089);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AgaA7QgGAAgDgDQgDgDAAgFQAAgDACgFIAFgHIAdgeQAGgDAEgFQAEgFACgFQACgEAAgFQAAgGgCgDQgCgEgEgDQgEgCgEAAQgJAAgHAJIgCAHQgCAFgCADQgCADgFAAQgEAAgCgDQgDgCAAgFQAAgFADgGQACgGAFgFQAEgFAIgDQAHgDAJAAQAMAAAHADQAFADAFAFIAGAKQACAGAAAGQAAAJgFAJQgFAIgEAEIgQAPQgMALgDAEIgEAFIAnAAQAFAAAEADQADADAAAEQAAAEgDADQgCACgFAAg");
	this.shape_46.setTransform(131.2,43.9,1.09,1.089);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AgSBLIAKgYQAEgKACgNQACgPAAgNQAAgMgCgPQgDgOgDgIIgKgZQAAAAAAgBQABAAAAgBQAAAAABAAQABAAABAAQAFAAADACQACABAFAKIAIAQIAFAPIAEAQIABAQQAAASgFAPQgEAPgJAQQgFAKgCABQgCACgGAAQgBAAgBAAQgBAAAAAAQAAgBgBAAQAAAAAAgBg");
	this.shape_47.setTransform(118.7,45.7,1.09,1.089);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AgXA2QgIgFgEgHQgDgIAAgEQAAgDADgDQACgDAEAAQAFAAAFAIQACAIAGADQAEAEAHAAQAFAAAEgDQAFgDACgGQADgGgBgHQAAgHgCgGQgDgGgEgBQgFgDgEAAQgFAAgEACIgIAFQgGAEgDAAQgDAAgEgDQgCgCAAgDIAHgsQAAgGAEgFQADgDAGAAIAoAAQALAAAAAKQAAADgCAEQgEADgFgBIgjAAIgFAaQALgGAJAAQAGAAAHAEQAGACAFAGQAEAEADAGQADAGAAAKQAAALgEAJQgGALgJAFQgJAGgLgBQgNABgKgGg");
	this.shape_48.setTransform(110.9,44.1,1.09,1.089);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AgXA2QgHgFgEgHQgEgHAAgFQAAgDADgDQACgDADAAQAHAAADAIQADAHAFAEQAFAEAHAAQAFAAAEgDQAEgDADgGQACgEAAgJQAAgIgCgFQgDgFgEgCQgEgDgFAAQgGAAgDACIgIAFQgGAEgDAAQgEAAgDgDQgCgCAAgDIAHgsQABgFACgGQADgDAHAAIAnAAQAMAAAAAKQAAAEgDADQgDADgFgBIgjAAIgFAaQALgGAIAAQAHAAAHAEQAFABAGAHQAFAEADAGQACAIAAAIQAAAMgFAIQgEAKgKAGQgKAGgKgBQgOABgJgGg");
	this.shape_49.setTransform(100.9,44.1,1.09,1.089);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AgTA2QgKgHgEgNQgDgGAAgIQgCgJAAgJQAAgLACgKQABgLACgFQAEgLAJgHQAJgGALgBQAHAAAHADQAFACAGAGQAGAHACAGQAGANAAAYQAAANgCAJQgBAKgFAHQgEALgJAEQgIAGgKgBQgKAAgJgGgAgNghQgEAMAAAVQAAAQACAIQABALAFAEQADAFAGAAQAHAAAEgFQAEgGACgJQABgJAAgPQAAgPgBgJQgCgJgEgFQgFgFgGAAQgJAAgEAKg");
	this.shape_50.setTransform(90.7,44,1.09,1.089);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AAJBNQgCgBgCgDIgFgJQgGgKgEgLQgEgKgCgKQgCgKAAgNQAAgMACgKQACgLAEgJQAFgMAFgJIAFgJIAFgDIAFgBQABAAABAAQAAAAABAAQAAABABAAQAAABAAAAIgKAaQgEAJgCAMQgDAOAAANQAAAOADAOQACALAEALIAKAZQAAABAAAAQgBAAAAABQgBAAAAAAQgBAAgBAAg");
	this.shape_51.setTransform(82.8,45.7,1.09,1.089);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AgGApQgDgDAAgHIAAg+QAAgGADgDQADgEADAAQAEAAADAEQADADAAAGIAAA+QAAAHgDADQgDAEgEAAQgDAAgDgEg");
	this.shape_52.setTransform(203.1,75.6,1.09,1.089);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AgYApQgDgDAAgHIAAg8QAAgPAKAAQAFAAACAEQACADAAAHIAIgKQAFgEAEAAQAGAAAHAEQAGAEAAAFQAAADgDAEQgCACgDAAIgNgDQgEAAgDADQgDACgBAFIgCAMIgBAiQAAAHgDADQgCAEgFAAQgEAAgDgEg");
	this.shape_53.setTransform(197.8,75.6,1.09,1.089);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AAXApQgEgEgCgFIgPAKQgFADgJAAQgIAAgGgEQgHgDgCgGQgDgGAAgGQAAgJAFgHQAFgFAKgCIAigJQgBgJgDgDQgCgFgJAAQgIAAgDADQgEACgDAFIgEAGQgBABgEAAQgEAAgCgCQgDgDABgDQAAgGADgFQAEgFAIgEQAIgEAMAAQAOAAAGAEQAIADADAHQAEAIAAAMIAAAYQAAAHACAGIABAJQAAADgDADQgDADgEAAQgCAAgEgEgAACAEIgKADQgEABgDADQgCADAAAEQgBAFAEAEQADADAGAAQAGAAAEgDQAFgDACgEQADgFABgLIAAgEIgOAEg");
	this.shape_54.setTransform(188.6,75.6,1.09,1.089);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AgGA6QgDgFAAgFIAAhfQAAgGADgEQADgDADAAQAEAAADADQADADAAAHIAABfQAAAHgDADQgDADgEAAQgDAAgDgDg");
	this.shape_55.setTransform(181.4,73.8,1.09,1.089);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("AAoApQgDgDAAgHIAAgvQgBgFgCgDQgDgDgFAAQgEAAgEADQgDACgCAFQgCAFAAAOIAAAdQAAAHgDADQgEAEgEAAQgDAAgEgEQgCgEAAgGIgBgvQAAgFgDgDQgCgDgFAAQgKAAgDAIQgDAHAAANIAAAeQAAAHgDADQgEAEgEAAQgFAAgCgEQgDgDAAgHIAAg+QAAgHACgCQACgEAFAAQAEAAADADQADADAAAGIAAACQAHgIAEgCQAGgDAIAAQAHAAAGADQAFAEADAGQAGgIAFgCQAGgDAHAAQAJAAAGADQAGADADAHQACAHAAAMIAAAqQAAAHgDADQgDAEgEAAQgEAAgEgEg");
	this.shape_56.setTransform(171.8,75.6,1.09,1.089);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("AgGApQgDgDAAgHIAAg+QAAgGADgDQACgEAEAAQAEAAADAEQADADAAAGIAAA+QAAAHgDADQgDAEgEAAQgEAAgCgEg");
	this.shape_57.setTransform(162.2,75.6,1.09,1.089);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFFFFF").s().p("AghA7QgFgCAAgGQAAgEADgBQABgDAFAAIAGACQAEAAACgCIAEgEIAFgNIgZhDQgCgFAAgEIABgFQABgCADgCIAFgBQAEAAACADQADADABAGIAQA1IAUg7QACgEACgBQABgBAEAAQAAAAABAAQABAAAAAAQABABAAAAQABAAAAAAQABABAAAAQABABAAAAQABABAAAAQAAABAAAAIACAFIgBAEIgdBQIgGAPQgEAGgEADQgFADgKAAQgHAAgGgCg");
	this.shape_58.setTransform(155.3,77.4,1.09,1.089);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFFFFF").s().p("AAXApQgDgEgDgFIgOAKQgGADgJAAQgJAAgEgEQgHgDgDgGQgDgGAAgGQAAgJAFgHQAGgFAJgCIAhgJQAAgJgDgDQgCgFgJAAQgGAAgGADQgDACgDAFIgEAGQgCABgDAAQgEAAgCgCQgCgDAAgDQAAgGADgFQAEgFAIgEQAHgEANAAQANAAAIAEQAGADAEAHQADAIAAAMIAAAYQAAAHADAGIABAJQAAADgDADQgDADgDAAQgDAAgEgEgAADAEIgMADQgDABgCADQgDADgBAEQAAAFAEAEQADADAGAAQAFAAAFgDQAGgDACgEQACgFAAgLIAAgEIgMAEg");
	this.shape_59.setTransform(146.1,75.6,1.09,1.089);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFFFFF").s().p("AghA7QgFgCABgGQAAgEACgBQACgDAEAAIAGACQAEAAACgCIADgEIAGgNIgZhDIgCgJIABgFIADgEIAGgBQAEAAADADQACACABAHIAQA1IAUg7IAEgFIAEgBQABAAABAAQAAAAABAAQAAABABAAQAAAAABAAIADAEIABAFIgCAKIgbBKIgHAPQgCAGgFADQgFADgJAAQgIAAgGgCg");
	this.shape_60.setTransform(136.8,77.4,1.09,1.089);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFFFFF").s().p("AgYApQgDgDAAgHIAAg8QAAgPAKAAQAFAAADAEQABACABAIQADgGAEgEQAEgEAFAAQAGAAAHAEQAGAEAAAFQAAADgCAEQgDACgDAAIgNgDQgEAAgDADQgCACgCAFIgCAMIgBAiQAAAHgCADQgDAEgFAAQgEAAgDgEg");
	this.shape_61.setTransform(125.1,75.6,1.09,1.089);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFFFFF").s().p("AAXApQgEgEgCgFIgOAKQgGADgJAAQgJAAgEgEQgGgDgEgGQgDgGAAgGQAAgJAFgHQAFgFAKgCIAhgJQAAgIgDgEQgDgFgIAAQgHAAgFADQgDACgDAFIgEAGQgCABgDAAQgEAAgCgCQgDgDABgDQAAgGADgFQAEgFAIgEQAIgEAMAAQAOAAAGAEQAIADADAHQAEAIAAAMIAAAYQAAAHACAGIABAJQAAADgDADQgCADgEAAQgDAAgEgEgAADAEIgLADQgEABgCADQgDADAAAEQgBAFAEAEQADADAGAAQAFAAAFgDQAFgDACgEQADgFAAgLIAAgEIgMAEg");
	this.shape_62.setTransform(115.9,75.6,1.09,1.089);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFFFFF").s().p("AATApQgDgDAAgHIAAgjQAAgLgDgGQgDgGgIAAQgFAAgEAEQgFAEgCAFQgBAGAAAMIAAAbQAAAIgDACQgDAEgEAAQgEAAgEgEQgCgEAAgGIAAg+QAAgHACgDQADgDAEAAQABAAAAAAQABAAABABQAAAAABAAQAAAAABABQADACAAACQACADAAAEIAAACQAFgHAGgEQAHgEAHAAQAIAAAHAEQAFAEAEAHIADAJIAAAzQAAAHgDADQgCAEgFAAQgEAAgDgEg");
	this.shape_63.setTransform(106,75.6,1.09,1.089);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFFFFF").s().p("AgGA5QgDgEAAgGIAAg+QAAgHADgDQACgDAEAAQAEAAADADQADADAAAGIAAA/QAAAGgDAEQgDADgEABQgEgBgCgDgAgGgpQgDgDAAgFQAAgEADgDQADgEADAAQAEAAADADQADADAAAFQAAAFgDADQgDADgEAAQgDAAgDgDg");
	this.shape_64.setTransform(98.7,73.9,1.09,1.089);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFFFFF").s().p("AgMBNIgHgCQAAAAgBAAQAAAAAAAAQAAgBAAAAQAAAAAAgBIgBgCQAAAAAAAAQAAgBABAAQAAgBAAAAQAAgBABAAIAFgBIACABIAHAAQALgBAAgGQAAgDgDgBQgCgCgCgBIgHACIgDgCIgBgDQAAgFAMgBIgKgCQgEAAgGgDIgJgHQgHgGgDgGIgHgRQgCgHAAgMQAAgPAEgKQAFgNAGgGQAHgJAKgEQAKgEAJAAQAOAAAKAFQALAHAFAIQAFAHAAAJQAAAEgCADQgDADgEAAQgDAAgCgBIgFgGIgFgKQgDgFgFgCQgFgDgIAAQgMAAgIALQgIALAAAVQAAAOADAIQAEAJAGAFQAHAEAIAAQAKAAAHgGQAHgGACgIQADgJAHAAQAEgBADADQADACAAAFQAAAFgCAEQgDAIgDADIgJAKQgKAIgSABIAAACQAIABADADQAFADAAAHQAAAFgDADQgDAEgGACQgFACgIAAg");
	this.shape_65.setTransform(90.3,75.6,1.09,1.089);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFFFFF").s().p("AgTA2IACgKIAXhmQACgCAEAAQAEAAACACQACACAAADIgWBlIgCAHIgCAEQgCACgDAAQgIAAAAgHg");
	this.shape_66.setTransform(77.6,73.8,1.09,1.089);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFFFFF").s().p("AhUBrQgJAAgGgHQgHgGAAgJIAAiqQAAgIAHgGQAGgHAJAAICqAAQAIAAAGAHQAHAGgBAIIAACqQABAJgHAGQgGAHgIAAgAhUBQQAAAFAFAAICfAAQAGAAAAgFIAAhaIgXAAIABAKQAAAagTATQgSATgbAAQgZAAgTgTQgTgTAAgaQAAgGABgEIgWAAgAgdgdQgNAMAAARQAAARANANQAMANARAAQASAAANgNQAMgNAAgRQAAgRgMgMQgNgNgSAAQgRAAgMANgAA1hPIAAAWQAAAEAGAAIAVAAQAGAAAAgEIAAgWQAAgGgGAAIgVAAQgGAAAAAGg");
	this.shape_67.setTransform(29.6,74.3,1.09,1.089);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FFFFFF").s().p("AAnBrIAAhcIAaAAIADgfIgdAAIAAgTQAAgJAIAAIAUAAIAAgfIgcAAQgUgBgKAOQgIAKAAAPIAAAVIgRAAIAAAfIARAAIAABcIhOAAQgMAAgIgJQgJgIAAgNIAAiaQAAgMAJgIQAIgJAMAAICaAAQAMAAAJAJQAJAIAAAMIAACaQAAANgJAIQgJAJgMAAg");
	this.shape_68.setTransform(58.3,74.3,1.09,1.089);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFFFFF").s().p("AggATIgBgBQgBgBAAgBQAAAAAAgBQABAAAAgBQAAAAABgBIAYgLQAXgLAPgIQAAgBABAAQABAAAAAAQABAAAAAAQABABAAABQABAAAAABQAAAAAAABQAAAAgBABQAAAAgBABQgMAHgbAMIgXAMIgCAAIgBAAg");
	this.shape_69.setTransform(73.5,15.9,1.09,1.089);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFFFFF").s().p("AgpgBIgBgBIgCgDQABgBAAAAQAAgBABAAQAAAAABAAQAAAAABAAIAhAEIAxAFQAAAAABAAQAAAAABABQAAAAAAABQAAAAABABQgBABAAABQAAAAAAABQgBAAAAAAQgBAAgBAAg");
	this.shape_70.setTransform(74.6,18.2,1.09,1.089);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FFFFFF").s().p("Ag8BqIgBgDQAEgjghg9QAAgBgBAAQAAgBAAAAQAAgBABAAQAAgBABgBQABAAAAAAQABAAABAAQAAABABAAQAAAAABABQAgA5gCAmQARgDAfgRIASgJQAWgMBfggQgigGhkgbIhlgcIgCgCQgag3gIgIQgBgBAAAAQAAgBgBAAQAAgBABAAQAAgBAAAAQAAAAABgBQAAAAAAAAQABAAAAAAQABgBAAAAQAQAEBXAnIArATQAbAMBuA3IABABIAAAAIABACIgBACIgBAAIAAAAIg4ATQg4AUgQAIIgSAKQgiASgUADgAhngpIBhAcQBaAXAlAJQhfgwgWgIIgrgUQhDgggagIQAPAYAOAgg");
	this.shape_71.setTransform(86.6,13.6,1.09,1.089);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#FFFFFF").s().p("AgXApIAAAAQgBAAAAgBQAAAAAAgBQAAAAAAgBQAAAAAAgBIAMgXQATgjANgSQAAgBABAAQAAgBABAAQAAAAABAAQAAABABAAQABABAAAAQAAABAAAAQABABgBABQAAAAAAABQgMAQgTAkIgNAXQAAABAAAAQAAAAgBAAQAAAAAAABQgBAAAAAAIgCgBg");
	this.shape_72.setTransform(72.5,13.5,1.09,1.089);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#FFFFFF").s().p("AgLADQhAgKgnAAIgCAAIgBgDQAAAAAAgBQAAAAABgBQAAAAABAAQAAgBABAAQAkAABEALQAqAFATACQAkADAbgCQABAAABABQABAAAAAAQABABAAAAQAAABAAAAQAAABAAABQAAAAgBABQAAAAgBABQAAAAgBAAIgSABQgpAAhDgLg");
	this.shape_73.setTransform(89.5,15.3,1.09,1.089);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24}]}).to({state:[{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24}]},123).to({state:[{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24}]},14).to({state:[]},18).wait(74));

	// Layer 4
	this.movieClip_1_1 = new lib.Symbol1();
	this.movieClip_1_1.parent = this;
	this.movieClip_1_1.setTransform(445,41);
	new cjs.ButtonHelper(this.movieClip_1_1, 0, 1, 1);

	this.instance_7 = new lib.flash0ai();
	this.instance_7.parent = this;
	this.instance_7.setTransform(-17,-23);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.movieClip_1_1}]}).to({state:[{t:this.instance_7}]},123).wait(106));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(433,27,1299.7,527.8);
// library properties:
lib.properties = {
	width: 900,
	height: 100,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"cinar.png?1506547367048", id:"cinar"}
	],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;