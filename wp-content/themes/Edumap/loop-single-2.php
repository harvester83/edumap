<?php
/**
 * single Post template 2
 **/

if (have_posts()) {
    the_post();

    $td_mod_single = new td_module_single($post);

    ?>
		
        <div class="td-post-content">
            <?php
            // override the default featured image by the templates (single.php and home.php/index.php - blog loop)
            if (!empty(td_global::$load_featured_img_from_template)) {
                echo $td_mod_single->get_image(td_global::$load_featured_img_from_template);
            } else {
                echo $td_mod_single->get_image('td_696x0');
            }
            ?>
			
			<?php echo $td_mod_single->get_title();?>
            <?php echo $td_mod_single->get_content();?>
            <?php echo $td_mod_single->get_social_sharing_top();?>
        </div>
    <?php echo $td_mod_single->related_posts();?>

<?php
} else {
    //no posts
    echo td_page_generator::no_posts();
}