<?php
class td_category_template_1 extends td_category_template {

    function render() {
        ?>

        <!-- subcategory -->
        <div class="td-category-header td-container-wrap">
            <div class="td-container">
                <div class="td-pb-row">
                    <div class="td-pb-span12">
                            <?php echo parent::get_description(); ?>

                    </div>
                </div>
                <?php echo parent::get_pull_down(); ?>
            </div>
        </div>

        <?php
    }
}
