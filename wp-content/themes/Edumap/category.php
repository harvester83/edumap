<?php get_header();

$pg = ($paged == 0)? 0 : $paged - 1;
$offset = 14 * $pg;
?>
    <div class="container _padding-top-20">
        <div class="row">
            <?php
            query_posts('posts_per_page=4&order=DESC&cat='.$cat.'&offset='.$offset);

            if( have_posts() ): while ( have_posts() ) : the_post(); ?>
                <div class="col-md-3 col-6 post-block-wrapper gutter-width-s">
                    <div class="post-block">
                        <div class="wrap wrap-owerflow">
                            <a href="<?php the_permalink(); ?>">
                                <?php if (has_post_thumbnail()){ ?>
                                    <?php the_post_thumbnail('medium', ['class' => 'post-block__img', 'title' => 'Feature image']);?>
                                <?php } else { ?>
                                    <img class="post-block__img" src="<?php echo catch_first_image(); ?>" alt="#">
                                <?php } ?>
                            </a>

                            <div class="date date_post-position">
                                <span class="post-block__date-day text-uppercase block"><?php echo get_the_date('j F');?></span>
                                <span class="post-block__date-time block"><?php echo get_the_date('H:i');?></span>
                            </div>
                        </div>

                        <div class="post-block__header">
                            <a class="post-block__link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

                            <div class="post-block__meta justify-content-between">
                                <span class="text-uppercase">
                                    <i class="fa fa-folder-open meta-icon" aria-hidden="true"></i>
                                    <span class="meta-value"><?php echo get_the_category($post->ID)[0]->name ?></span>
                                </span>

                                <span><i class="fa fa-eye meta-icon" aria-hidden="true"></i><span class="meta-value"><?php if(function_exists('the_views')) { the_views(); } ?></span></span>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>

        <div class="row">
            <div class="col gutter-width-s">
                <div class="banner">
                    <?php dynamic_sidebar('sidebar4'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="container _padding-top-20">
        <div class="row">
            <div class="posts-banner-section col gutter-width-s">
                <div class="row">
                    <?php


                    query_posts('posts_per_page=2&cat='.$cat.'&order=DESC&offset='.abs($offset+4)); ?>
                    <?php $i = 1; if( have_posts() ): while ( have_posts() ) : the_post(); ?>
                        <div class="post-block col <?php echo ($i % 2 == 0 )? 'post-block_even' : 'post-block_odd'; ?>">
                            <div class="wrap">
                                <a href="<?php the_permalink(); ?>">
                                    <?php if (has_post_thumbnail()){ ?>
                                        <?php the_post_thumbnail('medium', ['class' => 'post-block__img', 'title' => 'Feature image']);?>
                                    <?php } else { ?>
                                        <img class="post-block__img" src="<?php echo catch_first_image(); ?>" alt="#">
                                    <?php } ?>
                                </a>

                                <div class="date date_post-position">
                                    <span class="post-block__date-day text-uppercase block"><?php echo get_the_date('j F');?></span>
                                    <span class="post-block__date-time block"><?php echo get_the_date('H:i');?></span>
                                </div>
                            </div>

                            <div class="post-block__header">
                                <a class="post-block__link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

                                <div class="post-block__meta justify-content-between">
                                        <span class="text-uppercase">
                                            <i class="fa fa-folder-open meta-icon" aria-hidden="true"></i>
                                            <span class="meta-value"><?php echo get_the_category($post->ID)[0]->name ?></span>
                                        </span>
                                    <span><i class="fa fa-eye meta-icon" aria-hidden="true"></i><span class="meta-value">145</span></span>
                                </div>
                            </div>
                        </div>
                        <?php $i++; endwhile; ?>
                        <?php wp_reset_postdata(); ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="posts-banner-section col gutter-width-s">
                <div class="facebook-block">
                    <div class="fb-page"
                         data-href="https://www.facebook.com/edumap.az/"
                         data-hide-cover="true"
                         data-width="540"
                         data-show-facepile="true"></div>
                </div>

                <div class="banner">
                    <?php dynamic_sidebar('sidebar5'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="container _padding-top-20">
        <div class="row">
            <?php query_posts('posts_per_page=8&cat='.$cat.'&order=DESC&offset='.abs($offset+6)); ?>
            <?php if( have_posts() ): while ( have_posts() ) : the_post(); ?>
                <div class="col-md-3 post-block-wrapper gutter-width-s">
                    <div class="post-block">
                        <div class="wrap wrap-owerflow">
                            <a href="<?php the_permalink(); ?>">
                                <?php if (has_post_thumbnail()){ ?>
                                    <?php the_post_thumbnail('medium', ['class' => 'post-block__img', 'title' => 'Feature image']);?>
                                <?php } else { ?>
                                    <img class="post-block__img" src="<?php echo catch_first_image(); ?>" alt="#">
                                <?php } ?>
                            </a>

                            <div class="date date_post-position">
                                <span class="post-block__date-day text-uppercase block"><?php echo get_the_date('j F');?></span>
                                <span class="post-block__date-time block"><?php echo get_the_date('H:i');?></span>
                            </div>
                        </div>

                        <div class="post-block__header">
                            <a class="post-block__link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

                            <div class="post-block__meta justify-content-between">
                                <span class="text-uppercase">
                                    <i class="fa fa-folder-open meta-icon" aria-hidden="true"></i>
                                    <span class="meta-value"><?php echo get_the_category($post->ID)[0]->name ?></span>
                                </span>

                                <span><i class="fa fa-eye meta-icon" aria-hidden="true"></i><span class="meta-value"><?php if(function_exists('the_views')) { the_views(); } ?></span></span>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php endif; ?>

            <div class="col-12 gutter-width-s _padding-top-50 text-center pagination-block">
                <?php


                if(function_exists('wp_paginate')):
                    wp_paginate(false, $paged);
                else :
                    the_posts_pagination( array(
                        'prev_text'          => __( 'Previous page', 'twentysixteen' ),
                        'next_text'          => __( 'Next page', 'twentysixteen' ),
                        'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>',
                    ) );
                endif;
                ?>
            </div>
        </div>
    </div>
<?php get_footer();