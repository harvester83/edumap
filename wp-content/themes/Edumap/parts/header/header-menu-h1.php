<div id="td-header-menu" role="navigation">
    <div id="td-top-mobile-toggle"><a href="#"><i class="td-icon-font td-icon-mobile"></i></a></div>
    <div class="td-main-menu-logo td-logo-in-menu">
        <?php
        if (td_util::get_option('tds_logo_menu_upload') == '') {
            locate_template('parts/header/logo-h1.php', true, false);
        } else {
            locate_template('parts/header/logo-mobile-h1.php', true, false);
        }?>
    </div>
</div>

<div class="td-search-wrapper">
    <div id="td-top-search">
        <!-- Search -->
        <div class="header-search-wrap">
            <div class="dropdown header-search">
				<a href="#axtarish" ><i class="td-icon-search" style="color:white"></i></a>&nbsp;&nbsp;&nbsp; <font style="color:white">|</font> &nbsp;&nbsp;&nbsp;
				<a href="https://www.facebook.com/Edumap.az" target="_balnk"><i class="td-icon-facebook" style="color:white"></i></a>&nbsp;&nbsp;&nbsp;
				<a href="https://twitter.com/edumapaz/" target="_balnk"><i class="td-icon-twitter" style="color:white"></i></a>&nbsp;&nbsp;&nbsp;
				<a href="https://instagram.com/edumap.az/" target="_balnk"><i class="td-icon-instagram" style="color:white"></i></a>&nbsp;&nbsp;&nbsp;
				<a href="https://linkedin.com/in/edumap.az/" target="_balnk"><i class="td-icon-linkedin" style="color:white"></i></a>&nbsp;&nbsp;&nbsp;
            </div> 
        </div>
    </div>
</div>

<div class="header-search-wrap">
	<div class="dropdown header-search">
		<div class="td-drop-down-search" aria-labelledby="td-header-search-button">
			<form method="get" class="td-search-form" action="<?php echo esc_url(home_url( '/' )); ?>">
				<div role="search" class="td-head-form-search-wrap">
					<input id="td-header-search" type="text" value="<?php echo get_search_query(); ?>" name="s" autocomplete="off" /><input class="wpb_button wpb_btn-inverse btn" type="submit" id="td-header-search-top" value="<?php _etd('Search', TD_THEME_NAME)?>" />
				</div>
			</form>
			<div id="td-aj-search"></div>
		</div>
	</div>
</div>


<div id="axtarish" class="axtarish">
	<div class="popup">
		<h2>Axtarış</h2>
		<a class="close" href="#">&times;</a>
		<div class="content">
			<?php get_search_form(); ?>
		</div>
	</div>
</div>