<!--
Header style 12
-->
<div class="td-header-wrap td-header-style-12">
	<center><a href="http://itstep.az" target="_blank"><img src="https://edumap.az/wp-content/uploads/2017/11/step2.jpg" width="1000" ></img></a></center>
	<div class="td-header-menu-wrap-full td-container-wrap <?php echo td_util::get_option('td_full_menu'); ?>">
		<div class="td-header-menu-wrap td-header-gradient">
			<div class="td-container td-header-row td-header-main-menu">
				<?php locate_template('parts/header/header-menu-h1.php', true);?>
			</div>
		</div>
	</div>
	<ul>
	  <li class="dropdown">
		<a href="https://edumap.az/category/x%c9%99b%c9%99rl%c9%99r/" class="dropbtn">Xəbərlər</a>
		<div class="dropdown-content">
		  <a href="https://edumap.az/category/elm-v%c9%99-t%c9%99hsil/">Elm və Təhsil</a>
		  <a href="https://edumap.az/category/m%c9%99d%c9%99niyy%c9%99t/">Mədəniyyət</a>
		  <a href="https://edumap.az/category/maraqli/">Maraqlı</a>
		  <a href="https://edumap.az/category/faydali/">Faydalı</a>
		  <a href="https://edumap.az/category/idman/">İdman</a>
		  <a href="https://edumap.az/category/foto/">Foto</a>
		</div>
	  </li>
	  <li class="dropdown">
		<a href="https://edumap.az/category/t%c9%99dbirl%c9%99r/" class="dropbtn">Tədbirlər</a>
		<div class="dropdown-content">
		  <a href="https://edumap.az/category/oz%c9%99l-t%c9%99dbirl%c9%99r/">Özəl Tədbirlər</a>
		  <a href="https://edumap.az/category/xarici-tedbirler/">Xaricdə Tədbirlər</a>
		  <a href="https://edumap.az/category/layih%c9%99l%c9%99r/">Layihələr</a>
		  <a href="https://edumap.az/category/t%c9%99liml%c9%99r/">Təlimlər</a>
		  <a href="https://edumap.az/category/seminarlar/">Seminarlar</a>
		  <a href="https://edumap.az/category/musabiq%c9%99l%c9%99r/">Müsabiqələr</a>
		  <a href="https://edumap.az/category/s%c9%99rgil%c9%99r/">Sərgilər</a>
		  <a href="https://edumap.az/category/t%c9%99dbirl%c9%99r/konfranslar/">Konfranslar</a>
		  <a href="https://edumap.az/category/turlar/">Turlar</a>
		  <a href="https://edumap.az/category/od%c9%99nisli/">Ödənişli</a>
		  <a href="https://edumap.az/category/od%c9%99nissiz/">Ödənişsiz</a>
		</div>
	  </li>
	  <li class="dropdown">
		<a href="https://edumap.az/category/is-elanlari/" class="dropbtn">İş Elanları</a>
		<div class="dropdown-content">
		  <a href="https://edumap.az/category/t%c9%99crub%c9%99-proqramlari/">Təcrübə proqramları</a>
		  <a href="https://edumap.az/category/konullu-is/">Könüllü iş</a>
		</div>
	  </li>
	  <li><a href="https://edumap.az/category/xaricd%c9%99-t%c9%99hsil/">Xaricdə Təhsil</a></li>
	  <li class="dropdown">
		<a href="https://edumap.az/category/kurslar/" class="dropbtn">Kurslar</a>
		<div class="dropdown-content">
		  <a href="https://edumap.az/category/aminat-language-school/">Aminat Language School</a>
		  <a href="https://edumap.az/category/online-edumap-kursu/">Online Edumap Kursu</a>
		</div>
	  </li>
	  <li><a href="https://edumap.az/category/dil-oyr%c9%99nirik/">Dil Öyrənirik</a></li>
	  <li class="dropdown">
		<a href="https://edumap.az/category/dig%c9%99r/" class="dropbtn">Digər</a>
		<div class="dropdown-content">
		  <a href="https://edumap.az/category/kitabxana/">Kitabxana</a>
		  <a href="https://edumap.az/category/edumap-az-kitab-klubu/">Edumap Kitab Klubu</a>
		  <a href="https://edumap.az/category/yazarlar/">Yazarlar</a>
		  <a href="https://edumap.az/category/ecoart/">EcoArt</a>
		</div>
	  </li>
	  <li class="dropdown">
		<a href="https://edumap.az/category/t%c9%99qvim/" class="dropbtn">Təqvim</a>
		<div class="dropdown-content">
		  <a href="https://edumap.az/category/bazar-ert%c9%99si/">Bazar ertəsi</a>
		  <a href="https://edumap.az/category/c%c9%99rs%c9%99nb%c9%99-axsami/">Çərşənbə axşamı</a>
		  <a href="https://edumap.az/category/c%c9%99rs%c9%99nb%c9%99/">Çərşənbə</a>
		  <a href="https://edumap.az/category/cum%c9%99-axsami/">Cümə axşamı</a>
		  <a href="https://edumap.az/category/cum%c9%99/">Cümə</a>
		  <a href="https://edumap.az/category/s%c9%99nb%c9%99/">Şənbə</a>
		  <a href="https://edumap.az/category/bazar/">Bazar</a>
		</div>
	  </li>
	</ul>
	<div class="td-header-top-menu-full td-container-wrap <?php echo td_util::get_option('td_full_top_bar'); ?>">
		<div class="td-container td-header-row td-header-top-menu">
            <?php td_api_top_bar_template::_helper_show_top_bar() ?>
		</div>
	</div>
</div>