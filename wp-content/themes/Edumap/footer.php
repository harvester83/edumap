
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <p class="paragraph">&copy; 2018  edumap.az - Bütün Hüquqları Qorunur</p>
                    </div>
                </div>
            </div>
        </footer>
        <?php wp_footer(); ?>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-3.2.1.min.js"></script>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/bootstrap.min.js"></script>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/main.js"></script>
    </body>
</html>
