<?php get_header(); ?>

    <div class="container _padding-top-20">
        <div class="row">
            <div class="col-md-6 col-sm-12 gutter-width-s">
                <div class="primary-post-single">
                    <?php query_posts('posts_per_page=1&cat=30'); ?>
                    <?php if( have_posts() ): while ( have_posts() ) : the_post(); ?>
                        <a href="<?php echo get_the_permalink(); ?>">
                            <?php if (has_post_thumbnail()){ ?>
                                <?php the_post_thumbnail('large', ['class' => 'img', 'title' => 'Feature image']);?>
                            <?php } else { ?>
                                <img class="img" src="<?php echo catch_first_image(); ?>" alt="#">
                            <?php } ?>
                        </a>

                        <div class="date date_post-single-position">
                            <span class="post-block__date-day text-uppercase block"><?php echo get_the_date('j F');?></span>
                            <span class="post-block__date-time block"><?php echo get_the_date('H:i');?></span>
                        </div>

                        <div class="post-header padding-xl">
                            <a href="<?php echo get_the_permalink(); ?>" class="post-header__title"><?php echo get_the_title(); ?></a>
                        </div>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 col-12 gutter-width-s">
                <div class="primary-post-block_wrapper">
                    <?php query_posts('posts_per_page=2&cat=30&offset=3'); ?>
                    <?php if( have_posts() ): while ( have_posts() ) : the_post(); ?>
                        <div class="primary-post-block__item">
                            <a href="<?php echo get_the_permalink(); ?>">
                                <?php if (has_post_thumbnail()){ ?>
                                    <?php the_post_thumbnail('medium', ['class' => 'img', 'title' => 'Feature image']);?>
                                <?php } else { ?>
                                    <img class="img" src="<?php echo catch_first_image(); ?>" alt="#">
                                <?php } ?>
                            </a>

                            <div class="post-header padding-m">
                                <a href="<?php echo get_the_permalink(); ?>" class="post-header__title"><?php echo get_the_title(); ?></a>
                            </div>
                        </div>

                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 col-12 gutter-width-s">
                <div class="primary-post-block__banner">
                    <?php dynamic_sidebar('sidebar1'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="container _padding-top-20">
        <div class="row">
            <div class="posts-banner-section col gutter-width-s">
                <div class="banner margin-bottom-20">
                    <?php dynamic_sidebar('sidebar3'); ?>
                </div>

                <div class="row">
                    <?php query_posts('posts_per_page=2&cat=30&offset=1'); ?>
                    <?php $i = 1; if( have_posts() ): while ( have_posts() ) : the_post(); ?>
                            <div class="post-block col-md-6 col-sm-12 <?php echo ($i % 2 == 0 )? 'post-block_even' : 'post-block_odd'; ?>">
                                <div class="wrap">
                                    <a href="<?php the_permalink(); ?>">
                                        <?php if (has_post_thumbnail()){ ?>
                                            <?php the_post_thumbnail('medium', ['class' => 'post-block__img', 'title' => 'Feature image']);?>
                                        <?php } else { ?>
                                            <img class="post-block__img" src="<?php echo catch_first_image(); ?>" alt="#">
                                        <?php } ?>
                                    </a>

                                    <div class="date date_post-position">
                                        <span class="post-block__date-day text-uppercase block"><?php echo get_the_date('j F');?></span>
                                        <span class="post-block__date-time block"><?php echo get_the_date('H:i');?></span>
                                    </div>
                                </div>

                                <div class="post-block__header">
                                    <a class="post-block__link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

                                    <div class="post-block__meta justify-content-between">
                                        <span class="text-uppercase">
                                            <i class="fa fa-folder-open meta-icon" aria-hidden="true"></i>
                                            <span class="meta-value"><?php echo get_the_category($post->ID)[0]->name ?></span>
                                        </span>
                                        <span><i class="fa fa-eye meta-icon" aria-hidden="true"></i><span class="meta-value"><?php if(function_exists('the_views')) { the_views(); } ?></span></span>
                                    </div>
                                </div>
                            </div>
                    <?php $i++; endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="posts-banner-section col gutter-width-s">
                <div class="banner">
                    <?php dynamic_sidebar('sidebar14'); ?>
                </div>

              <div class="facebook-block">
                  <div class="fb-page"
                       data-href="https://www.facebook.com/edumap.az/"
                       data-hide-cover="true"
                       data-show-facepile="true"></div>
              </div>

                <div class="banner">
                    <?php dynamic_sidebar('sidebar2'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <?php query_posts( array('posts_per_page' => 12,)); ?>
            <?php if( have_posts() ): while ( have_posts() ) : the_post(); ?>
                <div class="col-md-3 col-6 post-block-wrapper gutter-width-s">
                    <div class="post-block">
                        <div class="wrap">
                            <a href="<?php the_permalink(); ?>">
                                <?php if (has_post_thumbnail()){ ?>
                                    <?php the_post_thumbnail('medium', ['class' => 'post-block__img', 'title' => 'Feature image']);?>
                                <?php } else { ?>
                                    <img class="post-block__img" src="<?php echo catch_first_image(); ?>" alt="#">
                                <?php } ?>
                            </a>

                            <div class="date date_post-position">
                                <span class="post-block__date-day text-uppercase block"><?php echo get_the_date('j F');?></span>
                                <span class="post-block__date-time block"><?php echo get_the_date('H:i');?></span>
                            </div>
                        </div>

                        <div class="post-block__header">
                            <a class="post-block__link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

                            <div class="post-block__meta justify-content-between">
                                <span class="text-uppercase">
                                    <i class="fa fa-folder-open meta-icon" aria-hidden="true"></i>
                                    <span class="meta-value"><?php echo get_the_category($post->ID)[0]->name ?></span>
                                </span>

                                <span><i class="fa fa-eye meta-icon" aria-hidden="true"></i><span class="meta-value"><?php if(function_exists('the_views')) { the_views(); } ?></span></span>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>

<?php get_footer(); ?>
