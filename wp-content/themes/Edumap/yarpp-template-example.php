<div class="row">
    <?php if (have_posts()):?>
        <?php while (have_posts()) : the_post(); ?>
            <div class="col-md-3 col-6 post-block-wrapper gutter-width-s">
                <div class="post-block">
                    <div class="wrap">
                        <a href="<?php the_permalink(); ?>">
                            <?php if (has_post_thumbnail()){ ?>
                                <?php the_post_thumbnail('medium', ['class' => 'post-block__img', 'title' => 'Feature image']);?>
                            <?php } else { ?>
                                <img class="post-block__img" src="<?php echo catch_first_image(); ?>" alt="#">
                            <?php } ?>
                        </a>

                        <div class="date date_post-position">
                            <span class="post-block__date-day text-uppercase block"><?php echo get_the_date('j F');?></span>
                            <span class="post-block__date-time block"><?php echo get_the_date('H:i');?></span>
                        </div>
                    </div>

                    <div class="post-block__header">
                        <a class="post-block__link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

                        <div class="post-block__meta justify-content-between">
                            <span class="text-uppercase">
                                <i class="fa fa-folder-open meta-icon" aria-hidden="true"></i>
                                <span class="meta-value"><?php echo get_the_category($post->ID)[0]->name ?></span>
                            </span>
                            <span><i class="fa fa-eye meta-icon" aria-hidden="true"></i><span class="meta-value"><?php if(function_exists('the_views')) { the_views(); } ?></span></span>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
    <?php else: ?>
        <p>Oxşar xəbər tapılmadı.</p>
    <?php endif; ?>
</div>
