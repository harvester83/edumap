<?php get_header(); ?>
    <div class="container single-page _padding-top-20 posts-container">
        <div class="row">
            <?php if( have_posts() ): while ( have_posts() ) : the_post(); ?>
                <div class="col-md-8 gutter-width-s">
                    <div class="wrap">
                        <?php if (has_post_thumbnail()):?>
                            <?php the_post_thumbnail('large'); ?>

                            <div class="date date_post-single-position">
                                <span class="post-block__date-day text-uppercase block"><?php echo get_the_date('j F');?></span>
                                <span class="post-block__date-time block"><?php echo get_the_date('H:i');?></span>
                            </div>
                        <?php endif; ?>
                    </div>

                    <h2 class="title_single text-uppercase"><?php the_title();?></h2>

                    <div class="post-inner">
                        <?php the_content(); ?>
                    </div>
                </div>
            <?php endwhile; ?>
            <?php endif; ?>

            <div class="col-md-4 gutter-width-s">
                <div class="banner margin-bottom-20">
                    <?php dynamic_sidebar('sidebar6'); ?>
                </div>

                <div class="banner margin-bottom-20">
                    <?php dynamic_sidebar('sidebar7'); ?>
                </div>

                <div class="banner margin-bottom-20">
                    <?php dynamic_sidebar('sidebar8'); ?>
                </div>

                <div class="banner margin-bottom-20">
                    <?php dynamic_sidebar('sidebar9'); ?>
                </div>
            </div>
        </div>

        <h3 class="text-uppercase">Oxşar xəbərlər</h3>
        <?php related_posts(); ?>
    </div>
<?php get_footer(); ?>

