<!doctype html >
<!--[if IE 8]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
    <title><?php wp_title('|', true, 'right'); ?></title>
    <meta charset="<?php bloginfo( 'charset' );?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.min.css">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<!--Addvenures-->
<div class="banner-side banner-side_left">
    <div class="banner-side-inner banner-side-inner_left">
        <?php dynamic_sidebar('sidebar11'); ?>
    </div>
</div>

<div class="banner-side banner-side_right">
    <div class="banner-side-inner banner-side-inner_right">
        <?php dynamic_sidebar('sidebar12'); ?>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-12 gutter-width-s">
            <?php dynamic_sidebar('sidebar10'); ?>
        </div>
    </div>

    <div class="top-header">
        <div class="row top-header_background">
            <div class="col-md-4 col-sm-12 gutter-width-s ">
                <div class="logo">
                    <a href="<?php echo get_site_url(); ?>">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-white.png" alt="#">
                    </a>
                </div>
            </div>

            <div class="col-md-5 col-12 search-form_wrraper gutter-width-s ">
                <form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                    <div class="search-block">
                        <label class="screen-reader-text" for="s"><?php _x( 'Search for:', 'label' ); ?></label>
                        <input class="search__input" type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" />
                        <input type="submit" id="searchsubmit" value="<?php echo esc_attr_x( 'Search5', 'submit button' ); ?>" hidden />
                        <label class="submit-label" for="searchsubmit"><i class="fa fa-search fa-rotate-90" aria-hidden="true"></i></label>
                    </div>
                </form>
            </div>

            <div class="col-md-3 col-12 social-icon_wrap gutter-width-s ">
                <div class="soc-icons">
                    <a class="social__link" href="https://www.facebook.com/edumap.az/"><i class="fa fa-facebook social-icon" aria-hidden="true"></i></a>
                    <a class="social__link" href="https://twitter.com/edumapaz/"><i class="fa fa-twitter social-icon" aria-hidden="true"></i></a>
                    <a class="social__link" href="https://plus.google.com/u/0/108797376970126427611"><i class="fa fa-google-plus social-icon" aria-hidden="true"></i></a>
                    <a class="social__link" href="https://www.linkedin.com/in/rasim-abdurehmanov-45892766/"><i class="fa fa-linkedin social-icon" aria-hidden="true"></i></a>
                    <a class="social__link" href="https://instagram.com/edumap.az/"><i class="fa fa-instagram social-icon" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 gutter-width-s">
            <nav class="navbar navbar-toggleable-md navbar-expand-lg">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon">
                    <i class="fa fa-bars" aria-hidden="true"></i></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'primary',
                        'menu_id'        => 'primary-menu',
                        'container'      => false,
                        'depth'          => 2,
                        'menu_class'     => 'navbar-nav mr-auto',
                        'walker'         => new Bootstrap_NavWalker(),
                        'fallback_cb'    => 'Bootstrap_NavWalker::fallback',
                    ) );
                    ?>
                </div>
            </nav>
        </div>
    </div>
</div>
