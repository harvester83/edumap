<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'edumap');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'SUI3!LQhq0:s~~XdAN{^jSNLWdmV5SD2Mbpo!i)^=bbul{Dm5)}]A:eRlDy>_7Q)');
define('SECURE_AUTH_KEY',  'C}(.F~YcF:F85}q=5cGYT[k&eMF2xvuk9md1SEI0b`uT][KD)n4zzZRU s6:rIbF');
define('LOGGED_IN_KEY',    'S)L2iW?8(mN&+-OfnJ{lxwY#zdCHAJ_9qv$7z?SEu(q6o10Cf%@vMwry<|Z&dBZa');
define('NONCE_KEY',        '1M1 Q1=Ov)2+oKTGWKVkD G01>&1R ;r(s_)DXn8p}nCc^cX!l?)MytP%wu_#FW6');
define('AUTH_SALT',        'X9U1E@$FcZ:&AfElTp>ZzOmk*;w%.r{n}-zjr,QCI4n&#K:iHZe?2W=6%vc {,r0');
define('SECURE_AUTH_SALT', '}blFJ%z42oN^JnBxL=}xk!My@6jT[Hrlc@<(oLpaBrE(,&wb<5{W75QH|~!4orSt');
define('LOGGED_IN_SALT',   '*q|mIq;+;TQ6EVVdK1x]`T~#7u.8FaA-!r{^cPwM(voFfDdcnvoVP32ZJ zQc.{p');
define('NONCE_SALT',       '>o8>N)[ci`.NB!c3$~r7@kC8n~m4/:_^zs^!nqE;-@>M8o;H%Y@{|n3XJGSa2MSS');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
